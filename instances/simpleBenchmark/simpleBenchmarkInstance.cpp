#include <ExpressionGenerators.h>
#include <InstanceInterface.h>
#include <MilpModel.h>
#include <RangeSet.h>

#include <memory>

#include <fmt/core.h>

using namespace copt;

class SimpleBenchmarkInstance final : public InstanceInterface
{
public:
    SimpleBenchmarkInstance() = default;

    [[nodiscard]] MilpModel *instance() const override
    {
        return model_.get();
    }

    void buildModel() override
    {
        const int size = static_cast<int>(model_->P("size").value({}));

        RangeSet I(size);

        model_->addContinuousUnboundedVariable("x", I);

        model_->addConstraint(
            "NB1",
            [&I](const MilpModel &model, int i) {
                const auto &x_i = model.V("x", { i });
                return x_i >= 1;
            },
            I);

        model_->setObjective("obj", [&I](const MilpModel &model) {
            return fn::vsum(
                model, "x", [](int i) -> std::vector<std::variant<int, std::string>> { return { i }; }, I);
        });
    }

    [[nodiscard]] std::vector<std::string> expectedParameters() const override
    {
        return { "size" };
    }

    void setParameter(Parameter &&param) override
    {
        model_->setParameter(std::forward<Parameter>(param));
    }

private:
    std::unique_ptr<MilpModel> model_ = std::make_unique<MilpModel>();
};

#ifdef __cplusplus
extern "C" {
#endif

copt::InstanceInterface *createInstance()
{
    return new SimpleBenchmarkInstance;
}

void releaseInstance(copt::InstanceInterface *solver)
{
    delete solver;
}

#ifdef __cplusplus
};

#endif
