#include <ExpressionGenerators.h>
#include <InstanceInterface.h>
#include <MilpModel.h>
#include <RangeSet.h>

#include <memory>

#include <fmt/core.h>

using namespace copt;

class SimpleBenchmarkInstance final : public InstanceInterface
{
public:
    SimpleBenchmarkInstance() = default;

    [[nodiscard]] MilpModel *instance() const override
    {
        return model_.get();
    }

    void buildModel() override
    {
        const int m = static_cast<int>(model_->P("m").value({}));

        RangeSet I(1, m * m + 1);
        RangeSet J(1, m + 1);

        model_->addBinaryVariable("x", I, I, I);

        model_->addConstraint(
            "NB1",
            [&I](const MilpModel &model, int i, int k) {
                return fn::vsum(
                           model, "x",
                           [i, k](int j) -> std::vector<IdxType> {
                               return { i, j, k };
                           },
                           I) == 1;
            },
            I, I);

        model_->addConstraint(
            "NB2",
            [&I](const MilpModel &model, int j, int k) {
                return fn::vsum(
                           model, "x",
                           [j, k](int i) -> std::vector<IdxType> {
                               return { i, j, k };
                           },
                           I) == 1;
            },
            I, I);

        model_->addConstraint(
            "NB3",
            [](const MilpModel &model, int k, int p, int q) {
                const auto m = static_cast<int>(model.P("m").value({}));

                return fn::vsum(
                           model, "x",
                           [k](int i, int j) -> std::vector<IdxType> {
                               return { i, j, k };
                           },
                           RangeSet(m * p - m + 1, m * p + 1), RangeSet(m * q - m + 1, m * q + 1)) == 1;
            },
            I, J, J);

        model_->addConstraint(
            "NB4",
            [&I](const MilpModel &model, int i, int j) {
                return fn::vsum(
                           model, "x",
                           [i, j](int k) -> std::vector<IdxType> {
                               return { i, j, k };
                           },
                           I) == 1;
            },
            I, I);

        model_->addConstraint(
            "NB5",
            [](const MilpModel &model, int i, int j, int k) {
                const auto G = model.P("G");

                if (!G.hasIndex({ i, j }))
                {
                    return Constraint(Marker::Skip);
                }

                const auto G_i_j = G.value({ i, j });

                if (G_i_j == k)
                {
                    const auto &x_i_j_k = model.V("x", { i, j, k });

                    return x_i_j_k == 1.;
                }

                return Constraint(Marker::Skip);
            },
            I, I, I);

        model_->setObjective("obj", [](const MilpModel &model) { return 0.; });
    }

    [[nodiscard]] std::vector<std::string> expectedParameters() const override
    {
        return { "m", "G" };
    }

    void setParameter(Parameter &&param) override
    {
        model_->setParameter(std::forward<Parameter>(param));
    }

    void printSolutionToStdout() const override
    {
        const int m = static_cast<int>(model_->P("m").value({}));

        RangeSet I(1, m * m + 1);
        RangeSet J(1, m + 1);

        for (int i : I)
        {
            for (int j : I)
            {
                for (int k : I)
                {
                    const auto &x = model_->V("x", { i, j, k });

                    if (std::abs(x.solutionValue().value() - 1.) <= 1.E-6)
                    {
                        fmt::print("{} ", k);
                        break;
                    }
                }

                if (j % m == 0)
                {
                    fmt::print("  ");
                }
            }

            if (i % m == 0)
            {
                fmt::print("\n\n");
            }
            else
            {
                fmt::print("\n");
            }
        }
    }

private:
    std::unique_ptr<MilpModel> model_ = std::make_unique<MilpModel>();
};

#ifdef __cplusplus
extern "C" {
#endif

copt::InstanceInterface *createInstance()
{
    return new SimpleBenchmarkInstance;
}

void releaseInstance(copt::InstanceInterface *solver)
{
    delete solver;
}

#ifdef __cplusplus
};

#endif
