#include "SolverPluginManager.h"
#include "logger.h"

#include <cstdlib>
#include <dlfcn.h>
#include <filesystem>
#include <map>
#include <optional>
#include <string_view>

namespace copt::solverPlugins
{

namespace fs = std::filesystem;

namespace
{

constexpr auto pluginDirEnvVar = "COPT_PLUGINS_DIR";

std::optional<std::string> pluginFolder()
{
    if (const auto *dir = std::getenv(pluginDirEnvVar))
    {
        return dir;
    }

    return std::nullopt;
}

bool stringEndsWith(std::string_view str, std::string_view end)
{
    return str.size() >= end.size() && 0 == str.compare(str.size() - end.size(), end.size(), end);
}

void discoverPlugins(std::string_view pluginDir, std::map<std::string, void *> &plugins)
{
    auto lg = logger();

    if (!fs::exists(pluginDir))
    {
        lg->warn("the specified plugin folder {} does not exist", pluginDir);

        return;
    }

    for (const auto &p : fs::directory_iterator(pluginDir))
    {
        if (const auto &path = p.path(); path.extension() == ".so" && stringEndsWith(path.stem().string(), "-plugin"))
        {
            lg->info("found plugin in {}: {}", pluginDir, path.filename().string());

            void *loadedPlugin = dlopen(path.c_str(), RTLD_LAZY);

            if (loadedPlugin)
            {
                if (auto *nameFunc = dlsym(loadedPlugin, SolverInterface::nameFuncName))
                {
                    auto *actualNamefunc = reinterpret_cast<SolverInterface::nameFuncType>(nameFunc);

                    plugins.insert({ actualNamefunc(), loadedPlugin });
                }
            }
        }
    }
}

} // namespace

struct SolverPluginManager::Impl
{
    std::optional<std::string>      pluginDir;
    std::map<std::string, void *>   plugins;
    std::shared_ptr<spdlog::logger> log;

    Impl()
        : pluginDir(pluginFolder())
        , log(logger())
    {}
};

SolverPluginManager::SolverPluginManager() noexcept
    : pimpl_(std::make_unique<Impl>())
{
    if (pimpl_->pluginDir)
    {
        pimpl_->log->info("looking for plugins in folder {}", *pimpl_->pluginDir);
        discoverPlugins(*pimpl_->pluginDir, pimpl_->plugins);
    }
    else
    {
        pimpl_->log->warn(
            "please define a valid path to a directory containing solver plugins in the environment variable {}",
            pluginDirEnvVar);
    }
}

SolverPluginManager::~SolverPluginManager()
{
    for (const auto &kv : pimpl_->plugins)
    {
        dlclose(kv.second);
    }
}

size_t SolverPluginManager::numPlugins() const
{
    return pimpl_->plugins.size();
}

std::vector<std::string> SolverPluginManager::pluginNames() const
{
    std::vector<std::string> names;
    names.reserve(pimpl_->plugins.size());

    std::transform(std::cbegin(pimpl_->plugins), std::cend(pimpl_->plugins), std::back_inserter(names),
                   [](const auto &kv) { return kv.first; });

    return names;
}

SolverInterface *SolverPluginManager::createSolver(const std::string &pluginName) const
{
    const auto &it = pimpl_->plugins.find(pluginName);

    if (it != pimpl_->plugins.cend())
    {
        if (auto *createFunc = dlsym(it->second, SolverInterface::createFuncName))
        {
            auto *actualCreateFunc = reinterpret_cast<SolverInterface::createInstanceFuncType>(createFunc);
            return actualCreateFunc();
        }
    }

    return nullptr;
}

void SolverPluginManager::releaseSolver(const std::string &pluginName, SolverInterface *solver) const
{
    const auto &it = pimpl_->plugins.find(pluginName);

    if (it != pimpl_->plugins.cend())
    {
        if (auto *releaseFunc = dlsym(it->second, SolverInterface::releaseFuncName))
        {
            auto actualReleaseFunc = reinterpret_cast<SolverInterface::releaseInstanceFuncType>(releaseFunc);
            actualReleaseFunc(solver);
        }
    }
    else
    {
        pimpl_->log->warn("could not find a plugin of name '{}', could not clean it up => this is a memory leak",
                          pluginName);
    }
}

} // namespace copt::solverPlugins
