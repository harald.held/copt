#ifndef LOGGER_H
#define LOGGER_H

#include <memory>

#include <spdlog/logger.h>

namespace copt::solverPlugins
{

std::shared_ptr<spdlog::logger> logger();

} // namespace copt::solverPlugins

#endif // LOGGER_H
