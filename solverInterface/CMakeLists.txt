# interface target #####################################################################################################
add_library(solverInterface INTERFACE)

target_include_directories(solverInterface INTERFACE
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:include/${PROJECT_NAME}>
    include)

target_compile_features(solverInterface INTERFACE cxx_std_17)

# plugin manager target ################################################################################################
set(PUBLIC_HEADERS
    include-pluginManager/SolverPluginManager.h
    )

set(SOURCES_AND_PRIVATE_HEADERS
    src/logger.cpp
    src/logger.h
    src/SolverPluginManager.cpp
    )

add_library(solverPluginManager STATIC ${PUBLIC_HEADERS} ${SOURCES_AND_PRIVATE_HEADERS})

set_target_properties(solverPluginManager PROPERTIES LINKER_LANGUAGE CXX)
set_target_properties(solverPluginManager PROPERTIES CXX_STANDARD 17)

target_link_libraries(solverPluginManager
    PRIVATE
    CONAN_PKG::spdlog
    dl
    PUBLIC
    solverInterface
    )

target_include_directories(solverPluginManager PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include-pluginManager>
    $<INSTALL_INTERFACE:include/solverPluginManager>
    include-pluginManager
    )

target_include_directories(solverPluginManager
    PRIVATE
    src
    )

# install information ##################################################################################################
install(TARGETS solverInterface
    RUNTIME DESTINATION bin
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib
    )

install(DIRECTORY include/ DESTINATION include/${PROJECT_NAME})
