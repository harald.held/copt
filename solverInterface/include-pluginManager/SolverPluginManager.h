#ifndef COPT_SOLVERPLUGINMANAGER_H
#define COPT_SOLVERPLUGINMANAGER_H

#include "SolverInterface.h"

#include <memory>
#include <string>
#include <vector>

namespace copt::solverPlugins
{

class SolverPluginManager
{
public:
    SolverPluginManager() noexcept;
    ~SolverPluginManager();

    [[nodiscard]] size_t                   numPlugins() const;
    [[nodiscard]] std::vector<std::string> pluginNames() const;

    [[nodiscard]] SolverInterface *createSolver(const std::string &pluginName) const;
    void                           releaseSolver(const std::string &pluginName, SolverInterface *solver) const;

private:
    struct Impl;
    std::unique_ptr<Impl> pimpl_;
};

} // namespace copt::solverPlugins

#endif // COPT_SOLVERPLUGINMANAGER_H
