#ifndef COPT_SOLVEROPTIONS_H
#define COPT_SOLVEROPTIONS_H

namespace copt
{

namespace Options
{

enum class OutputLevel
{
    None,    //< disable solver output
    Error,   //< only output errors and warnings
    Normal,  //< normal, i.e., solver default output
    Verbose, //< most verbose solver output for debugging
};

}

struct SolverOptions final
{
    Options::OutputLevel outputLevel = Options::OutputLevel::Normal;
};

} // namespace copt

#endif // COPT_SOLVEROPTIONS_H
