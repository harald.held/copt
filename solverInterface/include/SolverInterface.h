#ifndef COPT_SOLVERINTERFACE_H
#define COPT_SOLVERINTERFACE_H

#include "SolverOptions.h"

#include <string_view>
#include <vector>

namespace copt
{

enum class SolverStatus
{
    Feasible,   ///< feasible point found but not proven to be optimal
    Infeasible, ///< provenly infeasible
    Optimal     ///< found feasible solution that is proved by the solver to be optimal
};

enum class ObjectiveSense
{
    Minimize,
    Maximize
};

class SolverInterface
{
public:
    static constexpr auto nameFuncName    = "solverName";
    static constexpr auto createFuncName  = "createInstance";
    static constexpr auto releaseFuncName = "releaseInstance";

    using nameFuncType            = const char *(*)();
    using createInstanceFuncType  = SolverInterface *(*)();
    using releaseInstanceFuncType = void (*)(SolverInterface *);

    struct VarCoeff
    {
        double coefficient;
        size_t index;
    };

    inline virtual ~SolverInterface() = default;

    SolverStatus solve()
    {
        applySolverOptions();
        return runSolver();
    }

    // problem definition ##############################################################################################
    virtual void giveSizeHint(size_t numConstraints, size_t numVariables)
    {}

    virtual void addBinaryVariable(std::string_view name, size_t index)                           = 0;
    virtual void addIntegerVariable(std::string_view name, size_t index, double lb, double ub)    = 0;
    virtual void addContinuousVariable(std::string_view name, size_t index, double lb, double ub) = 0;

    virtual void addConstraint(std::string_view name, std::vector<VarCoeff> &&coeffs, size_t index, double lb,
                               double ub)                                                                     = 0;
    virtual void setObjective(std::string_view name, std::vector<VarCoeff> &&coeffs, ObjectiveSense objSense) = 0;

    // solver options ##################################################################################################
    SolverOptions &options()
    {
        return options_;
    }

    // solution retrieval ##############################################################################################
    [[nodiscard]] virtual double variableValue(size_t index) const = 0;

protected:
    SolverOptions options_;

    virtual void         applySolverOptions() = 0;
    virtual SolverStatus runSolver()          = 0;
};

} // namespace copt

#endif // COPT_SOLVERINTERFACE_H
