#include "LinearExpression.h"
#include "StringConversion.h"
#include "Variable.h"

#include <fmt/core.h>

#include <numeric>

namespace copt
{

template<>
std::string toString(const LinearExpression &le)
{
    const double constPart = le.constant();
    const auto & coeffs    = le.coeffs();

    std::string str = std::accumulate(std::cbegin(coeffs), std::cend(coeffs), std::string{},
                                      [](const std::string &s, const auto &kv) {
                                          const auto coeffStr = fmt::format("{} * {}", kv.second, kv.first->name());
                                          return s.empty() ? coeffStr : fmt::format("{} + {}", s, coeffStr);
                                      });

    if (constPart != 0.)
    {
        str += fmt::format(" + {}", constPart);
    }

    return str;
}

bool LinearExpression::VarComp::operator()(const Variable *v1, const Variable *v2) const
{
    return v1->name() < v2->name();
}

LinearExpression::LinearExpression() = default;

LinearExpression::LinearExpression(const Variable &v)
{
    coeffs_[&v] = 1.;
}

LinearExpression::LinearExpression(double a)
{
    constant_ = a;
}

LinearExpression &LinearExpression::operator+(const LinearExpression &other)
{
    constant_ += other.constant_;

    for (const auto it : other.coeffs_)
    {
        coeffs_[it.first] += it.second;
    }

    return *this;
}

LinearExpression LinearExpression::operator+(const LinearExpression &other) const
{
    LinearExpression le;

    le.constant_ = constant_;
    le.coeffs_   = coeffs_;

    le.addConstant(other.constant_);

    for (const auto it : other.coeffs_)
    {
        le.addToVarCoeff(it.first, it.second);
    }

    return le;
}

LinearExpression &LinearExpression::operator-(const LinearExpression &other)
{
    constant_ -= other.constant_;

    for (const auto it : other.coeffs_)
    {
        coeffs_[it.first] -= it.second;
    }

    return *this;
}

LinearExpression LinearExpression::operator-(const LinearExpression &other) const
{
    LinearExpression le;

    le.constant_ = constant_;
    le.coeffs_   = coeffs_;

    le.addConstant(-other.constant_);

    for (const auto it : other.coeffs_)
    {
        le.addToVarCoeff(it.first, -it.second);
    }

    return le;
}

LinearExpression LinearExpression::operator*(double v) const
{
    LinearExpression le;

    le.constant_ = v * constant_;

    for (const auto it : coeffs_)
    {
        le.coeffs_[it.first] = v * it.second;
    }

    return le;
}

double LinearExpression::constant() const
{
    return constant_;
}

void LinearExpression::addConstant(double v)
{
    constant_ += v;
}

auto LinearExpression::coeffs() const -> const CoeffMap &
{
    return coeffs_;
}

void LinearExpression::addToVarCoeff(const Variable *x, double v)
{
    coeffs_[x] += v;
}

LinearExpression operator*(double a, const Variable &v)
{
    LinearExpression le;

    le.addToVarCoeff(&v, a);

    return le;
}

LinearExpression operator+(double a, const Variable &v)
{
    LinearExpression le;

    le.addConstant(a);
    le.addToVarCoeff(&v, 1.);

    return le;
}

LinearExpression operator-(double a, const Variable &v)
{
    LinearExpression le;

    le.addConstant(a);
    le.addToVarCoeff(&v, -1.);

    return le;
}

LinearExpression operator+(const Variable &v1, const Variable &v2)
{
    LinearExpression le;

    le.addToVarCoeff(&v1, 1.);
    le.addToVarCoeff(&v2, 1.);

    return le;
}

LinearExpression operator-(const Variable &v1, const Variable &v2)
{
    LinearExpression le;

    le.addToVarCoeff(&v1, 1.);
    le.addToVarCoeff(&v2, -1.);

    return le;
}

} // namespace copt
