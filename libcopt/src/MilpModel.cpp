#include "MilpModel.h"
#include "logger.h"

#include <algorithm>
#include <numeric>

#include <fmt/core.h>

namespace copt
{

template<>
std::string toString(const std::string &t)
{
    return t;
}

template<>
std::string toString(const int &t)
{
    return std::to_string(t);
}

template<>
std::string toString(const std::variant<int, std::string> &t)
{
    if (std::holds_alternative<std::string>(t))
    {
        return std::get<std::string>(t);
    }
    else
    {
        return std::to_string(std::get<int>(t));
    }
}

template<>
std::string toString(const std::vector<std::variant<int, std::string>> &v)
{
    return std::accumulate(std::cbegin(v), std::cend(v), std::string{},
                           [](const std::string &s, const std::variant<int, std::string> &vv) {
                               return s.empty() ? toString(vv) : fmt::format("{}, {}", s, toString(vv));
                           });
}

template<>
std::string toString(const MilpModel::ObjectiveSense &os)
{
    switch (os)
    {
    case MilpModel::ObjectiveSense::Maximize:
        return "maximize";
    case MilpModel::ObjectiveSense::Minimize:
    default:
        return "minimize";
    }
}

namespace
{

auto log = logger();

std::string boundToString(double v)
{
    if (v == std::numeric_limits<double>::lowest())
    {
        return "-INF";
    }

    if (v == std::numeric_limits<double>::max())
    {
        return "INF";
    }

    return fmt::format("{}", v);
}

} // namespace

MilpModel::MilpModel() = default;

MilpModel::~MilpModel() = default;

void MilpModel::pprint() const
{
    if (!params_.empty())
    {
        fmt::print("Parameters:\n==========\n");

        for (const auto &kv_param : params_)
        {
            fmt::print("parameter {}\n----------\n", kv_param.first);

            for (const auto &param : kv_param.second.allValues())
            {
                fmt::print("index: ({:>4}): {}\n", toString(param.first), param.second);
            }

            fmt::print("\n");
        }

        fmt::print("\n");
    }

    fmt::print("Objective '{}' (sense: {}):\n==========\n", objName_, toString(objSense_));

    fmt::print("{}\n", toString(obj_));

    fmt::print("Variables (total no.: {}):\n==========\n", vars_.size());

    for (const auto &kv_var : varMap_)
    {
        const auto &varName = kv_var.first;

        fmt::print("variable {}\n----------\n", varName);

        if (kv_var.second.size() > 1)
        {
            // in this case, the variable is indexed
            for (const auto &kv_idx : kv_var.second)
            {
                const auto &    varIdx = kv_idx.first;
                size_t          vecIdx = kv_idx.second;
                const Variable &var    = *vars_[vecIdx];

                const auto varType = toString(var.varType());

                fmt::print("index: ({:>4}): {} <= {} <= {} [{}]\n", toString(varIdx), boundToString(var.lb()),
                           var.name(), boundToString(var.ub()), varType);
            }
        }
        else
        {
            // here, the variable has no index
            const Variable &var     = *vars_[kv_var.second.at({})];
            const auto      varType = toString(var.varType());

            fmt::print("{} <= {} <= {} [{}]\n", boundToString(var.lb()), var.name(), boundToString(var.ub()), varType);
        }
    }

    fmt::print("\n");

    fmt::print("Constraints (total no.: {}):\n==========\n", constraints_.size());

    for (const auto &kv_cstr : constraintMap_)
    {
        const auto &cstrName = kv_cstr.first;

        fmt::print("constraint {}\n----------\n", cstrName);

        if (kv_cstr.second.size() > 1)
        {
            // in this case, the constraint is indexed
            for (const auto &kv_idx : kv_cstr.second)
            {
                const auto &      cstrIdx = kv_idx.first;
                size_t            vecIdx  = kv_idx.second;
                const Constraint &cstr    = *constraints_[vecIdx];

                fmt::print("{} [index: ({:>4})]: {} <= {} <= {}\n", cstr.name(), toString(cstrIdx),
                           boundToString(cstr.lowerBound()), toString(cstr.linearExpression()),
                           boundToString(cstr.upperBound()));
            }
        }
        else
        {
            // here, the constraint has no index
            const Constraint &cstr = *constraints_[kv_cstr.second.at({})];

            fmt::print("{} <= {} <= {}\n", boundToString(cstr.lowerBound()), cstr.name(),
                       boundToString(cstr.upperBound()));
        }

        fmt::print("\n");
    }
}

size_t MilpModel::numVars() const
{
    return vars_.size();
}

const Variable &MilpModel::V(const std::string &name, const std::vector<IdxType> &idxs) const
{
    const auto idxInVarVec = varMap_.at(name).at(idxs);

    return *vars_[idxInVarVec];
}

std::vector<const Variable *> MilpModel::Vs(const std::string &name) const
{
    std::vector<const Variable *> vs;

    const auto &varsWithName = varMap_.at(name);

    for (const auto &v : varsWithName)
    {
        const auto idx = v.second;
        vs.push_back(vars_[idx].get());
    }

    return vs;
}

std::vector<std::string> MilpModel::varNames() const
{
    std::vector<std::string> allVarNames;

    allVarNames.reserve(allVarNames.size());

    std::transform(std::cbegin(varMap_), std::cend(varMap_), std::back_inserter(allVarNames),
                   [](const auto &kv) { return kv.first; });

    return allVarNames;
}

size_t MilpModel::varIndex(const Variable *var) const
{
    return varIndexMap_.at(var);
}

std::vector<Variable *> MilpModel::vars() const
{

    std::vector<Variable *> vs;
    vs.reserve(vars_.size());

    for (const auto &v : vars_)
    {
        vs.emplace_back(v.get());
    }

    return vs;
}

size_t MilpModel::numConstraints() const
{
    return constraints_.size();
}

const Constraint &MilpModel::C(const std::string &name, const std::vector<IdxType> &idxs) const
{
    const auto idxInCstrVec = constraintMap_.at(name).at(idxs);

    return *constraints_[idxInCstrVec];
}

std::vector<const Constraint *> MilpModel::cstrs(const std::string &name) const
{
    std::vector<const Constraint *> cs;

    const auto &cstrsWithName = constraintMap_.at(name);

    for (const auto &v : cstrsWithName)
    {
        const auto idx = v.second;
        cs.push_back(constraints_[idx].get());
    }

    return cs;
}

std::vector<std::string> MilpModel::constraintNames() const
{
    std::vector<std::string> allCstrNames;

    allCstrNames.reserve(allCstrNames.size());

    std::transform(std::cbegin(constraintMap_), std::cend(constraintMap_), std::back_inserter(allCstrNames),
                   [](const auto &kv) { return kv.first; });

    return allCstrNames;
}

size_t MilpModel::constraintIndex(const Constraint *cstr) const
{
    return constraintIndexMap_.at(cstr);
}

std::vector<Constraint *> MilpModel::constraints() const
{
    std::vector<Constraint *> cstrs;
    cstrs.reserve(constraints_.size());

    for (const auto &c : constraints_)
    {
        cstrs.emplace_back(c.get());
    }

    return cstrs;
}

void MilpModel::setObjective(const std::string &name, const MilpModel::ObjFunc &f)
{
    objName_ = name;
    obj_     = f(*this);
}

const LinearExpression::CoeffMap &MilpModel::objectiveCoefficients() const
{
    return obj_.coeffs();
}

std::string_view MilpModel::objectiveName() const
{
    return objName_;
}

MilpModel::ObjectiveSense MilpModel::objectiveSense() const
{
    return objSense_;
}

void MilpModel::setObjectiveSense(MilpModel::ObjectiveSense objSense)
{
    objSense_ = objSense;
}

void MilpModel::setParameter(Parameter &&param)
{
    params_.insert_or_assign(param.name(), std::move(param));
}

const Parameter &MilpModel::P(const std::string &name) const
{
    return params_.at(name);
}

} // namespace copt
