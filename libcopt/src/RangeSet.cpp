#include "RangeSet.h"

namespace copt
{

RangeSet::RangeSet(int from, int to)
    : from_(from)
    , to_(to)
{}

int RangeSet::count() const
{
    if (empty())
    {
        return 0;
    }

    return to_ - from_;
}

bool RangeSet::empty() const
{
    return to_ <= from_;
}

RangeSet::iterator RangeSet::begin() const
{
    return iterator(from_);
}

RangeSet::iterator RangeSet::end() const
{
    return iterator(to_);
}

} // namespace copt
