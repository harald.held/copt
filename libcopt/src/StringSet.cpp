#include "StringSet.h"

#include <utility>

namespace copt
{

StringSet::StringSet(std::vector<std::string> strings)
    : strings_(std::move(strings))
{}

int StringSet::count() const
{
    return strings_.size();
}

bool StringSet::empty() const
{
    return strings_.empty();
}

StringIterator StringSet::begin() const
{
    return iterator(strings_, 0);
}

StringIterator StringSet::end() const
{
    return iterator(strings_, strings_.size());
}

} // namespace copt
