#include "Variable.h"

namespace copt
{

Variable::Variable(std::string name, double lb, double ub, VariableType varType)
    : name_(std::move(name))
    , varType_(varType)
    , lb_(varType == VariableType::Binary ? 0. : lb)
    , ub_(varType == VariableType::Binary ? 1. : ub)
{}

std::string_view Variable::name() const
{
    return name_;
}

void Variable::setName(const std::string &name)
{
    name_ = name;
}

VariableType Variable::varType() const
{
    return varType_;
}

void Variable::setVarType(const VariableType &varType)
{
    varType_ = varType;
}

double Variable::lb() const
{
    return lb_;
}

void Variable::setLb(double lb)
{
    lb_ = lb;
}

double Variable::ub() const
{
    return ub_;
}

void Variable::setUb(double ub)
{
    ub_ = ub;
}

std::optional<double> Variable::solutionValue() const
{
    return solutionValue_;
}

void Variable::setSolutionValue(double v)
{
    solutionValue_ = v;
}

} // namespace copt
