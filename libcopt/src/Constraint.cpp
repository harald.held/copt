#include "Constraint.h"

namespace copt
{

Constraint::Constraint(std::string name)
    : name_(std::move(name))
{}

Constraint::Constraint(Marker marking)
    : marker_(marking)
{}

void Constraint::setLowerBound(double v)
{
    lb_ = v;
}

double Constraint::lowerBound() const
{
    return lb_;
}

void Constraint::setUpperBound(double v)
{
    ub_ = v;
}

double Constraint::upperBound() const
{
    return ub_;
}

const LinearExpression &Constraint::linearExpression() const
{
    return *le_;
}

void Constraint::setLinearExpression(std::unique_ptr<LinearExpression> le)
{
    le_ = std::move(le);
}

void Constraint::setName(const std::string &name)
{
    name_ = name;
}

std::string_view Constraint::name() const
{
    return name_;
}

std::optional<Marker> Constraint::marker() const
{
    return marker_;
}

Constraint operator<=(const LinearExpression &v, double ub)
{
    auto le = std::make_unique<LinearExpression>();

    for (const auto it : v.coeffs())
    {
        le->addToVarCoeff(it.first, it.second);
    }

    Constraint c;

    // we move the constant part to the RHS, i.e. the upper bound by subtracting it
    const double constantPart = v.constant();

    c.setUpperBound(ub - constantPart);
    c.setLinearExpression(std::move(le));

    return c;
}

Constraint operator>=(const LinearExpression &v, double lb)
{
    auto le = std::make_unique<LinearExpression>();

    for (const auto it : v.coeffs())
    {
        le->addToVarCoeff(it.first, it.second);
    }

    Constraint c;

    // we move the constant part to the RHS, i.e. the lower bound by subtracting it
    const double constantPart = v.constant();

    c.setLowerBound(lb - constantPart);
    c.setLinearExpression(std::move(le));

    return c;
}

Constraint operator==(const LinearExpression &v, double b)
{
    auto le = std::make_unique<LinearExpression>();

    for (const auto it : v.coeffs())
    {
        le->addToVarCoeff(it.first, it.second);
    }

    Constraint c;

    // we move the constant part to the RHS and LHS by subtracting it
    const double constantPart = v.constant();

    c.setLowerBound(b - constantPart);
    c.setUpperBound(c.lowerBound());
    c.setLinearExpression(std::move(le));

    return c;
}

} // namespace copt
