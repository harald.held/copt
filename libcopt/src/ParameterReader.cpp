#include "ParameterReader.h"
#include "Parameter.h"
#include "logger.h"

#include <yaml-cpp/yaml.h>

namespace copt
{

namespace
{

void extractParameter(const YAML::Node &valuesNode, Parameter &param, std::vector<IdxType> idxs)
{
    switch (valuesNode.Type())
    {
    case YAML::NodeType::Scalar:
        param.setValue(idxs, valuesNode.as<double>());
        break;
    case YAML::NodeType::Map: {
        idxs.emplace_back(IdxType{});

        for (YAML::const_iterator it_val = valuesNode.begin(); it_val != valuesNode.end(); ++it_val)
        {
            if (it_val->first.Tag() == "!")
            {
                // in this case, the node is a quoted and thus should be a string
                const auto idx = it_val->first.as<std::string>();
                idxs.back()    = idx;
            }
            else
            {
                // the only other index type we support is int
                const auto idx = it_val->first.as<int>();
                idxs.back()    = idx;
            }

            extractParameter(it_val->second, param, idxs);
        }
        break;
    }
    default:
        break;
    }
}

} // namespace

ParameterReader::ParameterReader(std::string ymlFilePath)
    : ymlFilePath_(std::move(ymlFilePath))
    , log_(logger())
{
    readParamFile();
}

void ParameterReader::readParamFile()
{
    YAML::Node paramYml = YAML::LoadFile(ymlFilePath_);

    log_->info("read model parameters from file '{}'", ymlFilePath_);

    if (!paramYml.IsMap() && !paramYml["Parameters"])
    {
        log_->error("the YAML file '{}' does not look as expected", ymlFilePath_);
        return;
    }

    const auto &paramsNode = paramYml["Parameters"];

    if (!paramsNode.IsSequence())
    {
        log_->error("parameters must be given as a sequence in the YAML file");
        return;
    }

    for (YAML::const_iterator it_param = paramsNode.begin(); it_param != paramsNode.end(); ++it_param)
    {
        const YAML::Node &currentParam = *it_param;

        if (!currentParam.IsMap() || currentParam.size() != 1)
        {
            log_->error("each parameter must be given as a map with exactly one key in the YAML file");
            return;
        }

        Parameter param(currentParam.begin()->first.as<std::string>());
        extractParameter(currentParam.begin()->second, param, {});
        params_.emplace_back(std::move(param));
    }
}

const std::vector<Parameter> &ParameterReader::params() const
{
    return params_;
}

} // namespace copt