#include "Iterator.h"

namespace copt
{

IntIterator::IntIterator(int start)
    : i(start)
{}

IntIterator &IntIterator::operator++()
{
    ++i;

    while (!std::all_of(std::cbegin(this->filters_), std::cend(this->filters_),
                        [this](const FilterType &filter) { return filter(i); }))
    {
        ++i;
    }

    return *this;
}

bool IntIterator::operator==(const IntIterator &other) const
{
    return i == other.i;
}

const int &IntIterator::operator*() const
{
    return i;
}

StringIterator::StringIterator(const std::vector<std::string> &strings, int start)
    : i(start)
    , strings_(strings)
{}

StringIterator &StringIterator::operator++()
{
    ++i;

    if (!good())
    {
        return *this;
    }

    while (!std::all_of(std::cbegin(this->filters_), std::cend(this->filters_),
                        [this](const FilterType &filter) { return filter(strings_.at(i)); }))
    {
        ++i;

        if (!good())
        {
            break;
        }
    }

    return *this;
}

bool StringIterator::operator==(const StringIterator &other) const
{
    return i == other.i;
}

const std::string &StringIterator::operator*() const
{
    return strings_.at(i);
}

bool StringIterator::good() const
{
    return i < strings_.size();
}
} // namespace copt