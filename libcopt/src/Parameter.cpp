#include "Parameter.h"

namespace copt
{

Parameter::Parameter(std::string name)
    : name_(std::move(name))
{}

const std::string &Parameter::name() const
{
    return name_;
}

void Parameter::setValue(const std::vector<IdxType> &idx, double value)
{
    values_[idx] = value;
}

double Parameter::value(const std::vector<IdxType> &idx) const
{
    return values_.at(idx);
}

const std::map<std::vector<IdxType>, double> &Parameter::allValues() const
{
    return values_;
}

bool Parameter::hasIndex(const std::vector<IdxType> &idx) const
{
    return values_.find(idx) != values_.cend();
}

} // namespace copt