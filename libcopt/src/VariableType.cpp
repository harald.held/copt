#include "VariableType.h"
#include "StringConversion.h"

namespace copt
{

template<>
std::string toString(const VariableType &vt)
{
    switch (vt)
    {
    case VariableType::Continuous:
        return "continuous";
    case VariableType::Binary:
        return "binary";
    case VariableType::Integer:
        return "integer";
    }

    return {};
}

} // namespace copt
