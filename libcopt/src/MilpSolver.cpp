#include "MilpSolver.h"
#include "MilpModel.h"
#include "StringConversion.h"
#include "VariableType.h"
#include "logger.h"

#include <SolverInterface.h>
#include <SolverPluginManager.h>

#include <chrono>
#include <iostream>

namespace copt
{

template<>
std::string toString(const SolverStatus &status)
{
    switch (status)
    {
    case SolverStatus::Optimal:
        return "optimal";
    case SolverStatus::Feasible:
        return "feasible";
    case SolverStatus::Infeasible:
        return "infeasible";
    }

    return {};
}

namespace
{

auto log = logger();

SolverStatus runSolver(SolverInterface *solver)
{
    auto startSolve = std::chrono::high_resolution_clock::now();

    const auto status = solver->solve();

    auto endSolve = std::chrono::high_resolution_clock::now();

    auto timeSolve = std::chrono::duration_cast<std::chrono::milliseconds>(endSolve - startSolve).count() / 1000.;

    log->info("solver finished after {} seconds and returned status {}", timeSolve, toString(status));

    return status;
}

} // namespace

MilpSolver::MilpSolver(const MilpModel &model)
    : model_(model)
    , spm_(std::make_unique<solverPlugins::SolverPluginManager>())
{}

MilpSolver::~MilpSolver() = default;

SolverOptions &MilpSolver::options()
{
    return solverOptions_;
}

std::vector<std::string> MilpSolver::availableSolvers()
{
    return spm_->pluginNames();
}

void MilpSolver::solve(const std::string &solverName) const
{
    auto *solver = spm_->createSolver(solverName);

    if (solver)
    {
        log->info("created instance of solver {}", solverName);
    }
    else
    {
        log->error("could not create an instance of solver {}", solverName);
        return;
    }

    buildSolverInstance(solver);
    solver->options() = solverOptions_;
    const auto status = runSolver(solver);

    if (status != SolverStatus::Infeasible)
    {
        importSolutionValues(solver);
    }

    spm_->releaseSolver(solverName, solver);
}

void MilpSolver::setupVariablesInSolver(SolverInterface *solver) const
{
    for (const auto *var : model_.vars())
    {
        const auto index = model_.varIndex(var);

        switch (var->varType())
        {
        case VariableType::Continuous:
            solver->addContinuousVariable(var->name(), index, var->lb(), var->ub());
            break;
        case VariableType::Integer:
            solver->addIntegerVariable(var->name(), index, var->lb(), var->ub());
            break;
        case VariableType::Binary:
            solver->addBinaryVariable(var->name(), index);
            break;
        }
    }
}

void MilpSolver::setupConstraintsInSolver(SolverInterface *solver) const
{
    for (const auto *cstr : model_.constraints())
    {
        const auto  index = model_.constraintIndex(cstr);
        const auto &le    = cstr->linearExpression();

        const auto &coeffMap = le.coeffs();

        std::vector<SolverInterface::VarCoeff> coeffs;
        coeffs.reserve(coeffMap.size());

        std::transform(std::cbegin(coeffMap), std::cend(coeffMap), std::back_inserter(coeffs), [this](const auto &kv) {
            const auto  *var   = kv.first;
            const double coeff = kv.second;

            return SolverInterface::VarCoeff{ coeff, model_.varIndex(var) };
        });

        solver->addConstraint(cstr->name(), std::move(coeffs), index, cstr->lowerBound(), cstr->upperBound());
    }
}

void MilpSolver::setupObjectiveInSolver(SolverInterface *solver) const
{
    const auto &coeffMap = model_.objectiveCoefficients();

    std::vector<SolverInterface::VarCoeff> coeffs;
    coeffs.reserve(coeffMap.size());

    std::transform(std::cbegin(coeffMap), std::cend(coeffMap), std::back_inserter(coeffs), [this](const auto &kv) {
        const auto  *var   = kv.first;
        const double coeff = kv.second;

        return SolverInterface::VarCoeff{ coeff, model_.varIndex(var) };
    });

    const ObjectiveSense os = [this]() {
        switch (model_.objectiveSense())
        {
        case MilpModel::ObjectiveSense::Maximize:
            return ObjectiveSense::Maximize;
        case MilpModel::ObjectiveSense::Minimize:
        default:
            return ObjectiveSense::Minimize;
        }
    }();

    solver->setObjective(model_.objectiveName(), std::move(coeffs), os);
}

void MilpSolver::buildSolverInstance(SolverInterface *solver) const
{
    log->info("start building model instance");

    auto startInstanceBuild = std::chrono::high_resolution_clock::now();

    solver->giveSizeHint(model_.numConstraints(), model_.numVars());

    setupVariablesInSolver(solver);
    setupConstraintsInSolver(solver);
    setupObjectiveInSolver(solver);

    auto endInstanceBuild = std::chrono::high_resolution_clock::now();

    auto timeInstanceBuild =
        std::chrono::duration_cast<std::chrono::milliseconds>(endInstanceBuild - startInstanceBuild).count() / 1000.;

    log->info("done building model instance (took {} seconds)", timeInstanceBuild);
}

void MilpSolver::importSolutionValues(SolverInterface *solver) const
{
    auto startImport = std::chrono::high_resolution_clock::now();

    // variable values #################################################################################################
    for (auto *var : model_.vars())
    {
        const auto index = model_.varIndex(var);

        const auto value = solver->variableValue(index);
        var->setSolutionValue(value);
    }

    // import done #####################################################################################################
    auto endImport = std::chrono::high_resolution_clock::now();

    auto timeImport = std::chrono::duration_cast<std::chrono::milliseconds>(endImport - startImport).count() / 1000.;

    log->info("imported solution values in {} seconds", timeImport);
}

} // namespace copt