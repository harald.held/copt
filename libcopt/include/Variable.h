#ifndef VARIABLE_H
#define VARIABLE_H

#include "VariableType.h"

#include <limits>
#include <optional>
#include <string>
#include <utility>

namespace copt
{

class Variable
{
public:
    /**
     * Creating a variable requires specifying a name \a name, a lower bound \a lb, an upper bound \a ub, and its type
     * \a varType.
     */
    Variable(std::string name, double lb, double ub, VariableType varType);

    /// Delegating constructor for unconstrained, continuous variable.
    explicit Variable(std::string name)
        : Variable(std::move(name), std::numeric_limits<double>::lowest(), std::numeric_limits<double>::max(),
                   VariableType::Continuous)
    {}

    /// Delegating constructor for unconstraint variable of any type.
    Variable(std::string name, VariableType varType)
        : Variable(std::move(name), std::numeric_limits<double>::lowest(), std::numeric_limits<double>::max(), varType)
    {}

    [[nodiscard]] std::string_view name() const;
    void                           setName(const std::string &name);

    [[nodiscard]] VariableType varType() const;
    void                       setVarType(const VariableType &varType);

    [[nodiscard]] double lb() const;
    void                 setLb(double lb);

    [[nodiscard]] double ub() const;
    void                 setUb(double ub);

    // solution retrieval ##############################################################################################
    [[nodiscard]] std::optional<double> solutionValue() const;
    void setSolutionValue(double v);

private:
    std::string           name_;
    VariableType          varType_;
    double                lb_;
    double                ub_;
    std::optional<double> solutionValue_;
};

} // namespace copt

#endif // VARIABLE_H
