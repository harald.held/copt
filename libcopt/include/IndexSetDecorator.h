#ifndef COPT_INDEXSETDECORATOR_H
#define COPT_INDEXSETDECORATOR_H

#include "IndexSet.h"

namespace copt
{

template<typename IndexType, typename IteratorType>
class IndexSetDecorator : public IndexSet<IndexType, IteratorType>
{
public:
    using iterator = IteratorType;

    explicit IndexSetDecorator(IndexSet<IndexType, IteratorType> *idxSet)
        : idxSet_(idxSet)
    {}

    [[nodiscard]] int count() const override
    {
        return idxSet_->count();
    }

    [[nodiscard]] bool empty() const override
    {
        return idxSet_->empty();
    }

    iterator begin() const override
    {
        return idxSet_->begin();
    }

    iterator end() const override
    {
        return idxSet_->end();
    }

protected:
    IndexSet<IndexType, IteratorType> *idxSet_;
};

} // namespace copt

#endif // COPT_INDEXSETDECORATOR_H
