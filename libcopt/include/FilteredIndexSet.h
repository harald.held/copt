#ifndef COPT_FILTEREDINDEXSET_H
#define COPT_FILTEREDINDEXSET_H

#include "IndexSetDecorator.h"
#include "Iterator.h"

#include <algorithm>
#include <functional>
#include <numeric>

namespace copt
{

template<typename IndexType, typename IteratorType>
class FilteredIndexSet : public IndexSetDecorator<IndexType, IteratorType>
{
public:
    using iterator   = IteratorType;
    using FilterFunc = std::function<bool(const IndexType &)>;

    FilteredIndexSet(IndexSet<IndexType, IteratorType> *idxSet, FilterFunc f)
        : IndexSetDecorator<IndexType, IteratorType>(idxSet)
        , filter_(std::move(f))
    {}

    [[nodiscard]] int      count() const override;
    [[nodiscard]] bool     empty() const override;
    [[nodiscard]] iterator begin() const override;
    [[nodiscard]] iterator end() const override;

private:
    FilterFunc filter_;
};

template<typename IndexType, typename IteratorType>
int FilteredIndexSet<IndexType, IteratorType>::count() const
{
    return std::accumulate(std::cbegin(*this->idxSet_), std::cend(*this->idxSet_), 0,
                           [this](int s, const IndexType &i) { return this->filter_(i) ? s + 1 : s; });
}

template<typename IndexType, typename IteratorType>
bool FilteredIndexSet<IndexType, IteratorType>::empty() const
{
    return !std::any_of(std::cbegin(*this->idxSet_), std::cend(*this->idxSet_),
                        [this](const IndexType &i) { return this->filter_(i); });
}

template<typename IndexType, typename IteratorType>
auto FilteredIndexSet<IndexType, IteratorType>::begin() const -> iterator
{
    if (empty())
    {
        return IndexSetDecorator<IndexType, IteratorType>::end();
    }

    auto it = IndexSetDecorator<IndexType, IteratorType>::begin();

    it.addFilter(filter_);

    while (!filter_(*it))
    {
        ++it;
    }

    return it;
}

template<typename IndexType, typename IteratorType>
auto FilteredIndexSet<IndexType, IteratorType>::end() const -> iterator
{
    auto it = IndexSetDecorator<IndexType, IteratorType>::end();

    if (empty())
    {
        return it;
    }

    it.addFilter(filter_);

    while (it.good() && !filter_(*it))
    {
        ++it;
    }

    return it;
}

/// template deduction guides for FilteredIndexSet
template<typename IndexType, typename IteratorType>
FilteredIndexSet(IndexSet<IndexType, IteratorType> *, typename FilteredIndexSet<IndexType, IteratorType>::FilterFunc)
    -> FilteredIndexSet<IndexType, IteratorType>;

} // namespace copt

#endif // COPT_FILTEREDINDEXSET_H
