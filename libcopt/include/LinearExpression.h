#ifndef LINEAREXPRESSION_H
#define LINEAREXPRESSION_H

#include <map>

namespace copt
{

class Variable;

class LinearExpression
{
public:
    struct VarComp
    {
        bool operator()(const Variable *v1, const Variable *v2) const;
    };

    using CoeffMap = std::map<const Variable *, double, VarComp>;

    LinearExpression();
    LinearExpression(const Variable &v);
    LinearExpression(double a);

    LinearExpression &operator+(const LinearExpression &other);
    LinearExpression  operator+(const LinearExpression &other) const;

    LinearExpression &operator-(const LinearExpression &other);
    LinearExpression  operator-(const LinearExpression &other) const;

    LinearExpression &operator+=(const LinearExpression &other)
    {
        return *this + other;
    }

    LinearExpression &operator-=(const LinearExpression &other)
    {
        return *this - other;
    }

    LinearExpression operator*(double v) const;

    [[nodiscard]] double constant() const;
    void                 addConstant(double v);

    [[nodiscard]] const CoeffMap &coeffs() const;
    void                          addToVarCoeff(const Variable *x, double v);

private:
    double   constant_ = 0.;
    CoeffMap coeffs_;
};

LinearExpression        operator*(double a, const Variable &v);
inline LinearExpression operator*(const Variable &v, double a)
{
    return a * v;
}
inline LinearExpression operator*(double a, const LinearExpression &le)
{
    return le * a;
}

LinearExpression        operator+(double a, const Variable &v);
inline LinearExpression operator+(const Variable &v, double a)
{
    return a + v;
}

LinearExpression        operator-(double a, const Variable &v);
inline LinearExpression operator-(const Variable &v, double a)
{
    return -a + v;
}

LinearExpression operator+(const Variable &v1, const Variable &v2);
LinearExpression operator-(const Variable &v1, const Variable &v2);

inline LinearExpression &operator+(const Variable &v, LinearExpression &other)
{
    return other + v;
}
inline LinearExpression &operator-(const Variable &v, LinearExpression &other)
{
    return -1. * other + v;
}
inline LinearExpression operator+(const Variable &v, const LinearExpression &other)
{
    return other + v;
}
inline LinearExpression operator-(const Variable &v, const LinearExpression &other)
{
    return -1. * other + v;
}

} // namespace copt

#endif // LINEAREXPRESSION_H
