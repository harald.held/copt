#ifndef COPT_LIBCOPT_INCLUDE_COMMONTYPES_H
#define COPT_LIBCOPT_INCLUDE_COMMONTYPES_H

#include <string>
#include <variant>

namespace copt
{

using IdxType = std::variant<int, std::string>;

}

#endif // COPT_LIBCOPT_INCLUDE_COMMONTYPES_H
