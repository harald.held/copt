#ifndef VARIABLETYPE_H
#define VARIABLETYPE_H

namespace copt
{

enum class VariableType
{
    Continuous,
    Binary,
    Integer
};

} // namespace copt

#endif // VARIABLETYPE_H
