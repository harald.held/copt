#ifndef COPT_INDEXSET_H
#define COPT_INDEXSET_H

namespace copt
{

template<typename IndexType, typename IteratorType>
class IndexSet
{
public:
    using iterator = IteratorType;

    [[nodiscard]] virtual int  count() const = 0;
    [[nodiscard]] virtual bool empty() const = 0;

    [[nodiscard]] virtual iterator begin() const = 0;
    [[nodiscard]] virtual iterator end() const   = 0;
};

} // namespace copt

#endif // COPT_INDEXSET_H
