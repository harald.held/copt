#ifndef CARTESIANPRODUCT_H
#define CARTESIANPRODUCT_H

#include <tuple>

namespace copt
{

namespace internal
{

template<typename F, typename... Ts>
void fmapCartesianProductHelper(F &&f, std::tuple<Ts...> t)
{
    std::apply(f, t);
}

template<typename F, typename Filter, typename... Ts>
void fmapCartesianProductHelperWithFilter(F &&f, Filter &&filter, std::tuple<Ts...> t)
{
    if (std::apply(filter, t))
    {
        std::apply(f, t);
    }
}

template<typename F, typename... Ts, typename RS, typename... RSs>
void fmapCartesianProductHelper(F &&f, std::tuple<Ts...> t, RS rs, RSs... tailRSs)
{
    for (auto it = std::begin(rs); it != std::end(rs); ++it)
    {
        fmapCartesianProductHelper(std::forward<F>(f), std::tuple_cat(t, std::tie(*it)), tailRSs...);
    }
}

template<typename F, typename Filter, typename... Ts, typename RS, typename... RSs>
void fmapCartesianProductHelperWithFilter(F &&f, Filter &&filter, std::tuple<Ts...> t, RS rs, RSs... tailRSs)
{
    for (auto it = std::begin(rs); it != std::end(rs); ++it)
    {
        fmapCartesianProductHelperWithFilter(std::forward<F>(f), std::forward<Filter>(filter),
                                             std::tuple_cat(t, std::tie(*it)), tailRSs...);
    }
}

} // namespace internal

template<typename F, typename... RSs>
void fmapCartesianProduct(F &&f, RSs... rss)
{
    internal::fmapCartesianProductHelper(std::forward<F>(f), std::make_tuple(), rss...);
}

template<typename F, typename Filter, typename... RSs>
void fmapCartesianProductWithFilter(F &&f, Filter &&filter, RSs... rss)
{
    internal::fmapCartesianProductHelperWithFilter(std::forward<F>(f), std::forward<Filter>(filter), std::make_tuple(),
                                                   rss...);
}

} // namespace copt

#endif // CARTESIANPRODUCT_H
