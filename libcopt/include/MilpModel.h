#ifndef MILPMODEL_H
#define MILPMODEL_H

#include "CartesianProduct.h"
#include "CommonTypes.h"
#include "Constraint.h"
#include "Parameter.h"
#include "StringConversion.h"
#include "Variable.h"
#include "VariableType.h"

#include <functional>
#include <map>
#include <memory>
#include <string>
#include <vector>

namespace copt
{

class MilpModel final
{
public:
    using BoundType = std::tuple<std::optional<double>, std::optional<double>>;

    enum class ObjectiveSense
    {
        Minimize,
        Maximize
    };

    MilpModel();
    ~MilpModel();

    /**
     * Print model information to stdout.
     */
    void pprint() const;

    // variable API ####################################################################################################
    template<typename... ISs>
    void addVariable(const std::string &name, double lb, double ub, VariableType varType, ISs... Is)
    {
        auto it_var = varMap_.find(name);

        if (it_var != varMap_.end())
        {
            throw std::runtime_error("cannot add variables with the same name more than once");
        }

        fmapCartesianProduct(
            [&name, lb, ub, varType, this](auto... i) {
                auto varName = [&name](auto... i) {
                    if constexpr (sizeof...(i) == 0)
                    {
                        return name;
                    }
                    else
                    {
                        return (name + ... + ("_" + toString(i)));
                    }
                }(i...);

                std::vector<IdxType> idxs;
                (idxs.push_back(i), ...);

                const auto newVarIdx = vars_.size();
                auto &     newVar    = vars_.emplace_back(std::make_unique<Variable>(varName, lb, ub, varType));

                varIndexMap_.insert({ newVar.get(), newVarIdx });

                auto &val = varMap_[name];
                val[idxs] = vars_.size() - 1;
            },
            Is...);
    }

    /**
     *
     * @tparam BoundFunc This is expected to be a function accepting as many indices as the variable has, i.e.
     *                   sizeof...(IS) many. In return, we expect a std::tuple<std::optional<double>,
     *                   std::optional<double>>, i.e. BoundType. These are then the lower and upper bound for the
     *                   variable with that index. If one of these values is std::nullopt, no corresponding bound is
     *                   imposed.
     * @tparam ISs
     * @param name
     * @param bounds
     * @param varType
     * @param Is
     */
    template<typename BoundFunc, typename... ISs>
    void addVariable(const std::string &name, BoundFunc &&bounds, VariableType varType, ISs... Is)
    {
        auto it_var = varMap_.find(name);

        if (it_var != varMap_.end())
        {
            throw std::runtime_error("cannot add variables with the same name more than once");
        }

        fmapCartesianProduct(
            [&name, &bounds, varType, this](auto... i) {
                auto varName = [&name](auto... i) {
                    if constexpr (sizeof...(i) == 0)
                    {
                        return name;
                    }
                    else
                    {
                        return (name + ... + ("_" + toString(i)));
                    }
                }(i...);

                std::vector<IdxType> idxs;
                (idxs.push_back(i), ...);

                const auto newVarIdx = vars_.size();

                // lb, ub are expected to be std::optional<double>
                const auto [lb, ub] = bounds(i...);

                auto &newVar = vars_.emplace_back(
                    std::make_unique<Variable>(varName, lb ? *lb : std::numeric_limits<double>::lowest(),
                                               ub ? *ub : std::numeric_limits<double>::max(), varType));

                varIndexMap_.insert({ newVar.get(), newVarIdx });

                auto &val = varMap_[name];
                val[idxs] = vars_.size() - 1;
            },
            Is...);
    }

    template<typename... ISs>
    void addUnboundedVariable(const std::string &name, VariableType varType, ISs... Is)
    {
        addVariable(name, std::numeric_limits<double>::lowest(), std::numeric_limits<double>::max(), varType, Is...);
    }

    template<typename... ISs>
    void addContinuousUnboundedVariable(const std::string &name, ISs... Is)
    {
        addUnboundedVariable(name, VariableType::Continuous, Is...);
    }

    template<typename... ISs>
    void addBinaryVariable(const std::string &name, ISs... Is)
    {
        addVariable(name, 0., 1., VariableType::Binary, Is...);
    }

    [[nodiscard]] size_t numVars() const;

    [[nodiscard]] const Variable &V(const std::string &name, const std::vector<IdxType> &idxs) const;
    [[nodiscard]] const Variable &V(const std::string &name) const
    {
        return V(name, {});
    }

    [[nodiscard]] std::vector<const Variable *> Vs(const std::string &name) const;
    [[nodiscard]] std::vector<std::string>      varNames() const;
    size_t                                      varIndex(const Variable *var) const;
    [[nodiscard]] std::vector<Variable *>       vars() const;

    // constraint API ##################################################################################################
    template<typename F, typename... ISs>
    void addConstraint(const std::string &name, F f, ISs... Is)
    {
        auto it_cstr = constraintMap_.find(name);

        if (it_cstr != constraintMap_.end())
        {
            throw std::runtime_error("cannot add constraints with the same name more than once");
        }

        fmapCartesianProduct(
            [&name, f, this](auto... i) {
                auto cstrName = [&name](auto... i) {
                    if constexpr (sizeof...(i) == 0)
                    {
                        return name;
                    }
                    else
                    {
                        return (name + ... + ("_" + toString(i)));
                    }
                }(i...);

                std::vector<IdxType> idxs;
                (idxs.push_back(i), ...);

                auto cstr = std::make_unique<Constraint>(f(*this, i...));

                if (cstr->marker() && *cstr->marker() == Marker::Skip)
                {
                    return;
                }

                cstr->setName(cstrName);

                const auto newCstrIdx = constraints_.size();
                auto &     newCstr    = constraints_.emplace_back(std::move(cstr));

                constraintIndexMap_.insert({ newCstr.get(), newCstrIdx });

                auto &val = constraintMap_[name];
                val[idxs] = constraints_.size() - 1;
            },
            Is...);
    }

    [[nodiscard]] size_t numConstraints() const;

    [[nodiscard]] const Constraint &C(const std::string &name, const std::vector<IdxType> &idxs) const;
    [[nodiscard]] const Constraint &C(const std::string &name) const
    {
        return C(name, {});
    }

    [[nodiscard]] std::vector<const Constraint *> cstrs(const std::string &name) const;
    [[nodiscard]] std::vector<std::string>        constraintNames() const;
    size_t                                        constraintIndex(const Constraint *cstr) const;
    [[nodiscard]] std::vector<Constraint *>       constraints() const;

    // objective API ###################################################################################################
    using ObjFunc = std::function<LinearExpression(const MilpModel &)>;

    void                                            setObjective(const std::string &name, const ObjFunc &f);
    [[nodiscard]] const LinearExpression::CoeffMap &objectiveCoefficients() const;
    [[nodiscard]] std::string_view                  objectiveName() const;
    [[nodiscard]] ObjectiveSense                    objectiveSense() const;
    void                                            setObjectiveSense(ObjectiveSense objSense);

    // parameter API ###################################################################################################
    /**
     * This adds a new parameter or overrides an already existing one with the given parameter.
     */
    void setParameter(Parameter &&param);

    const Parameter &P(const std::string &name) const;

private:
    std::vector<std::unique_ptr<Variable>> vars_;
    std::map<const Variable *, size_t>     varIndexMap_;

    std::vector<std::unique_ptr<Constraint>> constraints_;
    std::map<const Constraint *, size_t>     constraintIndexMap_;

    LinearExpression obj_;
    std::string      objName_;
    ObjectiveSense   objSense_ = ObjectiveSense::Minimize;

    /**
     * This maps a variable's name to a map from a vector of indices to the variable's index in \c vars_.
     */
    std::map<std::string, std::map<std::vector<IdxType>, size_t>> varMap_;

    /**
     * This maps a constraint's name to a map from a vector of indices to the constraint's index in \c constraints_.
     */
    std::map<std::string, std::map<std::vector<IdxType>, size_t>> constraintMap_;

    std::map<std::string, Parameter> params_;
};

} // namespace copt

#endif // MILPMODEL_H
