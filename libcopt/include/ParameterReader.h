#ifndef COPT_LIBCOPT_INCLUDE_PARAMETERREADER_H
#define COPT_LIBCOPT_INCLUDE_PARAMETERREADER_H

#include "Parameter.h"

#include <memory>
#include <string>
#include <vector>

namespace spdlog
{
class logger;
}

namespace copt
{

class ParameterReader
{
public:
    explicit ParameterReader(std::string ymlFilePath);

    [[nodiscard]] const std::vector<Parameter> &params() const;

private:
    std::string                     ymlFilePath_;
    std::shared_ptr<spdlog::logger> log_;
    std::vector<Parameter>          params_;

    void readParamFile();
};

} // namespace copt

#endif // COPT_LIBCOPT_INCLUDE_PARAMETERREADER_H
