#ifndef COPT_ITERATOR_H
#define COPT_ITERATOR_H

#include <functional>
#include <iterator>
#include <string>
#include <vector>

namespace copt
{

namespace internal
{

template<typename IndexType>
struct AcceptAllFilter
{
    constexpr bool operator()(const IndexType &) const noexcept
    {
        return true;
    }
};

} // namespace internal

template<typename IndexType, typename DerivedType>
class Iterator
{
public:
    using FilterType = std::function<bool(const IndexType &)>;

    virtual DerivedType &    operator++()                               = 0;
    virtual bool             operator==(const DerivedType &other) const = 0;
    virtual const IndexType &operator*() const                          = 0;

    [[nodiscard]] virtual bool good() const
    {
        return true;
    }

    bool operator!=(const DerivedType &other) const
    {
        return !(*this == other);
    }

    Iterator &addFilter(FilterType filter)
    {
        filters_.emplace_back(std::move(filter));
        return *this;
    }

protected:
    std::vector<FilterType> filters_{ internal::AcceptAllFilter<IndexType>() };
};

class IntIterator : public Iterator<int, IntIterator>
{
public:
    explicit IntIterator(int start = 0);

    IntIterator &operator++() override;
    bool         operator==(const IntIterator &other) const override;
    const int &  operator*() const override;

private:
    int i;
};

class StringIterator : public Iterator<std::string, StringIterator>
{
public:
    explicit StringIterator(const std::vector<std::string> &strings, int start = 0);

    StringIterator &   operator++() override;
    bool               operator==(const StringIterator &other) const override;
    const std::string &operator*() const override;

    [[nodiscard]] bool good() const override;

private:
    int                             i;
    const std::vector<std::string> &strings_;
};

} // namespace copt

namespace std
{

template<>
struct iterator_traits<copt::IntIterator>
{
    using iterator_category = std::forward_iterator_tag;
    using value_type        = int;
};

template<>
struct iterator_traits<copt::StringIterator>
{
    using iterator_category = std::forward_iterator_tag;
    using value_type        = std::string;
};

} // namespace std

#endif // COPT_ITERATOR_H
