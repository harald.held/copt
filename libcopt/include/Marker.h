#ifndef MARKER_H
#define MARKER_H

namespace copt
{

/// Enables to mark objects such as variables and cosntraints in certain ways.
enum class Marker
{
    Skip
};

} // namespace copt

#endif // MARKER_H
