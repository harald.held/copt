#ifndef EXPRESSIONGENERATORS_H
#define EXPRESSIONGENERATORS_H

#include "CartesianProduct.h"
#include "CommonTypes.h"
#include "LinearExpression.h"
#include "MilpModel.h"
#include "Parameter.h"

#include <string>
#include <vector>

namespace copt::fn
{

template<typename F, typename... ISs>
LinearExpression vsum(const MilpModel &model, const std::string &varName, F &&idx, ISs... Is)
{
    LinearExpression le;

    fmapCartesianProduct(
        [&le, &model, &varName, &idx](auto... i) {
            const auto &v = model.V(varName, idx(i...));
            le + v;
        },
        Is...);

    return le;
}

template<typename... ISs>
LinearExpression dot(const MilpModel &model, const std::string &paramName, const std::string &varName, ISs... Is)
{
    LinearExpression le;

    const auto param = model.P(paramName);

    fmapCartesianProduct(
        [&le, &param, &varName, &model](auto... i) {
            std::vector<IdxType> idxs;
            (idxs.push_back(i), ...);

            const auto &v      = model.V(varName, idxs);
            const auto  pValue = param.value(idxs);

            le + pValue *v;
        },
        Is...);

    return le;
}

/**
 * @tparam F a function used for index mapping. It should take as many argument of types as given by ISs (in form of
 * std::vector<IdxType>), and return a tuple of valid indices for the parameter and the variable, in that order
 * @tparam ISs
 * @param model
 * @param paramName
 * @param varName
 * @param idx
 * @param Is
 * @return
 */
template<typename F, typename... ISs>
LinearExpression dotMap(const MilpModel &model, const std::string &paramName, const std::string &varName, F &&idx,
                        ISs... Is)
{
    LinearExpression le;

    const auto param = model.P(paramName);

    fmapCartesianProduct(
        [&le, &param, &varName, &model, &idx](auto... i) {
            const auto [paramIdx, varIdx] = idx(i...);

            const auto &v      = model.V(varName, varIdx);
            const auto  pValue = param.value(paramIdx);

            le + pValue *v;
        },
        Is...);

    return le;
}

} // namespace copt::fn

#endif // EXPRESSIONGENERATORS_H
