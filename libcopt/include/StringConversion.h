#ifndef STRINGCONVERSION_H
#define STRINGCONVERSION_H

#include <string>

namespace copt
{

template<typename T>
std::string toString(const T &);

}

#endif // STRINGCONVERSION_H
