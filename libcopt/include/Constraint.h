#ifndef CONSTRAINT_H
#define CONSTRAINT_H

#include "LinearExpression.h"
#include "Marker.h"

#include <limits>
#include <memory>
#include <optional>
#include <stdexcept>
#include <string>

namespace copt
{

class Constraint
{
public:
    explicit Constraint(std::string name = {});
    explicit Constraint(Marker marking);

    void                 setLowerBound(double v);
    [[nodiscard]] double lowerBound() const;

    void                 setUpperBound(double v);
    [[nodiscard]] double upperBound() const;

    [[nodiscard]] const LinearExpression &linearExpression() const;
    void                                  setLinearExpression(std::unique_ptr<LinearExpression> le);

    void                           setName(const std::string &name);
    [[nodiscard]] std::string_view name() const;

    [[nodiscard]] std::optional<Marker> marker() const;

private:
    std::string                       name_;
    double                            lb_ = std::numeric_limits<double>::lowest();
    double                            ub_ = std::numeric_limits<double>::max();
    std::unique_ptr<LinearExpression> le_;
    std::optional<Marker>             marker_;
};

Constraint        operator<=(const LinearExpression &v, double ub);
inline Constraint operator>=(double ub, const LinearExpression &v)
{
    return v <= ub;
}

Constraint        operator>=(const LinearExpression &v, double lb);
inline Constraint operator<=(double lb, const LinearExpression &v)
{
    return v >= lb;
}

Constraint        operator==(const LinearExpression &v, double b);
inline Constraint operator==(double b, const LinearExpression &v)
{
    return v == b;
}

} // namespace copt

#endif // CONSTRAINT_H
