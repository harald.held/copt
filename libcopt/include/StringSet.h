#ifndef COPT_STRINGSET_H
#define COPT_STRINGSET_H

#include "IndexSet.h"
#include "Iterator.h"

#include <string>
#include <vector>

namespace copt
{

class StringSet : public IndexSet<std::string, StringIterator>
{
public:
    explicit StringSet(std::vector<std::string> strings);

    [[nodiscard]] int      count() const override;
    [[nodiscard]] bool     empty() const override;
    [[nodiscard]] iterator begin() const override;
    [[nodiscard]] iterator end() const override;

private:
    std::vector<std::string> strings_;
};

} // namespace copt

#endif // COPT_STRINGSET_H
