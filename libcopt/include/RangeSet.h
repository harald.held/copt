#ifndef RANGESET_H
#define RANGESET_H

#include "IndexSet.h"
#include "Iterator.h"

namespace copt
{

class RangeSet : public IndexSet<int, IntIterator>
{
public:
    /**
     * A RangeSet can be used to iterate over a range of integers without storing them actually.
     *
     * @param from beginning of the range
     * @param to end of the range, i.e. first number _not_ part of the range anymore
     */
    RangeSet(int from, int to);

    /**
     * This creates an index set with a specified number of elements. The indices are 0, 1, ..., numElements-1.
     *
     * @param numElements the number of elements in the RangeSet
     */
    explicit RangeSet(int numElements)
        : RangeSet(0, numElements)
    {}

    /**
     * @return the number of integers in the range
     */
    [[nodiscard]] int count() const override;

    /**
     * @return true iff the range is empty, i.e. there is no number in the range
     */
    [[nodiscard]] bool empty() const override;

    [[nodiscard]] iterator begin() const override;
    [[nodiscard]] iterator end() const override;

private:
    int from_;
    int to_;
};

} // namespace copt

#endif // RANGESET_H
