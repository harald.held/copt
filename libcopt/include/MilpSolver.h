#ifndef COPT_MILPSOLVER_H
#define COPT_MILPSOLVER_H

#include <SolverOptions.h>

#include <memory>
#include <string>
#include <vector>

namespace copt
{

class SolverInterface;
class MilpModel;

namespace solverPlugins
{
class SolverPluginManager;
}

class MilpSolver final
{
public:
    explicit MilpSolver(const MilpModel &model);
    ~MilpSolver();

    SolverOptions &options();
    void           solve(const std::string &solverName) const;

    [[nodiscard]] std::vector<std::string> availableSolvers();

private:
    const MilpModel &                                   model_;
    SolverOptions                                       solverOptions_;
    SolverInterface *                                   solver_ = nullptr;
    std::unique_ptr<solverPlugins::SolverPluginManager> spm_;

    void setupVariablesInSolver(SolverInterface *solver) const;
    void setupConstraintsInSolver(SolverInterface *solver) const;
    void setupObjectiveInSolver(SolverInterface *solver) const;
    void buildSolverInstance(SolverInterface *solver) const;
    void importSolutionValues(SolverInterface *solver) const;
};

} // namespace copt

#endif // COPT_MILPSOLVER_H
