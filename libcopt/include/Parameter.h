#ifndef COPT_LIBCOPT_INCLUDE_PARAMETER_H
#define COPT_LIBCOPT_INCLUDE_PARAMETER_H

#include "CommonTypes.h"

#include <map>
#include <string>
#include <vector>

namespace copt
{

class Parameter
{
public:
    explicit Parameter(std::string name);

    [[nodiscard]] const std::string &name() const;

    void                                                        setValue(const std::vector<IdxType> &idx, double value);
    [[nodiscard]] double                                        value(const std::vector<IdxType> &idx) const;
    [[nodiscard]] const std::map<std::vector<IdxType>, double> &allValues() const;
    [[nodiscard]] bool                                          hasIndex(const std::vector<IdxType> &idx) const;

private:
    std::string                            name_;
    std::map<std::vector<IdxType>, double> values_;
};

} // namespace copt

#endif // COPT_LIBCOPT_INCLUDE_PARAMETER_H
