#include <catch2/catch.hpp>

#include <ExpressionGenerators.h>
#include <RangeSet.h>

using namespace copt;

SCENARIO("constraints using indexed variables can be easily created via expression generator", "[expressionGenerators]")
{
    GIVEN("a MilpModel with a variable named 'x', with 2 indices")
    {
        MilpModel model;

        RangeSet I(0., 11.);
        RangeSet J(5., 15.);

        model.addContinuousUnboundedVariable("x", I, J);

        WHEN("adding constraints indexed by i in I, summing x[i, j] over j for each i")
        {
            model.addConstraint(
                "sum_xs",
                [&J](const MilpModel &model, int i) {
                    return fn::vsum(
                               model, "x",
                               [i](int j) -> std::vector<std::variant<int, std::string>> {
                                   return { i, j };
                               },
                               J) <= 3 * i;
                },
                I);

            THEN("the model has 10 constraints")
            {
                REQUIRE(model.numConstraints() == 11);
            }

            THEN("the constraints are what we put in")
            {
                const auto &constraints = model.cstrs("sum_xs");
                int         i           = 0;

                for (const auto *c : constraints)
                {
                    REQUIRE(c->lowerBound() == std::numeric_limits<double>::lowest());
                    REQUIRE(c->upperBound() == 3. * i);

                    const auto &le = c->linearExpression();

                    REQUIRE(le.constant() == 0.);

                    const auto &coeffs = le.coeffs();

                    REQUIRE(coeffs.size() == 10);

                    for (const auto &kv : coeffs)
                    {
                        REQUIRE(kv.second == 1.);
                    }

                    ++i;
                }
            }
        }
    }
}
