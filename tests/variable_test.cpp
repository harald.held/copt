#include <catch2/catch.hpp>

#include <Variable.h>

using namespace copt;

TEST_CASE("a new variable can be constructed by specifying a name, its type, a lower and an upper bound", "[variable]")
{
    Variable x("x", -1.1, 2.2, VariableType::Integer);

    REQUIRE(x.name() == "x");
    REQUIRE(x.varType() == VariableType::Integer);
    REQUIRE(x.lb() == -1.1);
    REQUIRE(x.ub() == 2.2);
}

TEST_CASE("a new, unbounded, continuous variable can be created by specifying only a name", "[variable]")
{
    Variable x("x");

    REQUIRE(x.name() == "x");
    REQUIRE(x.varType() == VariableType::Continuous);
    REQUIRE(x.lb() == std::numeric_limits<double>::lowest());
    REQUIRE(x.ub() == std::numeric_limits<double>::max());
}

TEST_CASE("a new unbounded variable can be created by name and type only", "[variable]")
{
    {
        Variable x("x", VariableType::Continuous);

        REQUIRE(x.name() == "x");
        REQUIRE(x.varType() == VariableType::Continuous);
        REQUIRE(x.lb() == std::numeric_limits<double>::lowest());
        REQUIRE(x.ub() == std::numeric_limits<double>::max());
    }

    {
        Variable x("x", VariableType::Binary);

        REQUIRE(x.name() == "x");
        REQUIRE(x.varType() == VariableType::Binary);
        REQUIRE(x.lb() == 0);
        REQUIRE(x.ub() == 1);
    }

    {
        Variable x("x", VariableType::Integer);

        REQUIRE(x.name() == "x");
        REQUIRE(x.varType() == VariableType::Integer);
        REQUIRE(x.lb() == std::numeric_limits<double>::lowest());
        REQUIRE(x.ub() == std::numeric_limits<double>::max());
    }
}
