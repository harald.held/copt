#include <catch2/catch.hpp>

#include <SolverPluginManager.h>

#include <cstdlib>
#include <fmt/core.h>

using namespace copt::solverPlugins;

// important note: these tests all assume that there is exactly one plugin compiled, and that it is the GLPK one
SCENARIO("plugins can be discovered from a directory specified in the COPT_PLUGINS_DIR environment variable",
         "[pluginManager]")
{
    GIVEN("a folder containing a solver plugin")
    {
        constexpr auto pluginDir = "../lib";

        WHEN("setting the environment variable COPT_PLUGIN_DIR to this folder")
        {
            setenv("COPT_PLUGINS_DIR", pluginDir, 1);

            AND_WHEN("instantiating a new SolverPluginManager")
            {
                SolverPluginManager spm;

                THEN("the plugin manager has discovered exactly one plugin")
                {
                    REQUIRE(spm.numPlugins() > 0);

                    AND_THEN("that plugin's name is 'glpk'")
                    {
                        const auto pluginNames = spm.pluginNames();

                        REQUIRE(pluginNames.size() == 2);

                        for (const auto &solverName : pluginNames)
                        {
                            AND_THEN(fmt::format("we can instantiate a {} solver instance", solverName))
                            {
                                auto *solver = spm.createSolver(solverName);

                                REQUIRE(solver != nullptr);

                                AND_THEN(fmt::format("we can release the {} instance again", solverName))
                                {
                                    REQUIRE_NOTHROW(spm.releaseSolver(pluginNames[0], solver));
                                }
                            }
                        }
                    }
                }
            }
        }

        WHEN("not setting the environment variable COPT_PLUGIN_DIR")
        {
            unsetenv("COPT_PLUGINS_DIR");

            AND_WHEN("instantiating a new SolverPluginManager")
            {
                SolverPluginManager spm;

                THEN("no plugins are found")
                {
                    REQUIRE(spm.numPlugins() == 0);
                }
            }
        }
    }

    GIVEN("a folder containing no solver plugins")
    {
        constexpr auto pluginDir = "doesNotExist";

        WHEN("setting the environment variable COPT_PLUGIN_DIR to this folder")
        {
            setenv("COPT_PLUGINS_DIR", pluginDir, 1);

            AND_WHEN("instantiating a new SolverPluginManager")
            {
                SolverPluginManager spm;

                THEN("the plugin manager has discovered 0 plugins")
                {
                    REQUIRE(spm.numPlugins() == 0);
                }
            }
        }
    }
}
