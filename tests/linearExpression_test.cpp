#include <catch2/catch.hpp>

#include <LinearExpression.h>
#include <Variable.h>

using namespace copt;

TEST_CASE("LinearExpressions behave as expected from algebra", "[linearExpression]")
{
    Variable x("x");
    Variable y("y");

    {
        LinearExpression le = x;

        const double constantPart = le.constant();
        const auto & coeffs       = le.coeffs();

        REQUIRE(constantPart == 0.);
        REQUIRE(coeffs.size() == 1);
        REQUIRE(coeffs.at(&x) == 1.);
    }

    {
        LinearExpression le = 1.;

        const double constantPart = le.constant();
        const auto & coeffs       = le.coeffs();

        REQUIRE(constantPart == 1.);
        REQUIRE(coeffs.empty());
    }

    {
        LinearExpression le = 1;

        const double constantPart = le.constant();
        const auto & coeffs       = le.coeffs();

        REQUIRE(constantPart == 1.);
        REQUIRE(coeffs.empty());
    }

    {
        LinearExpression le = 3. * x;

        const double constantPart = le.constant();
        const auto & coeffs       = le.coeffs();

        REQUIRE(constantPart == 0.);
        REQUIRE(coeffs.size() == 1);
        REQUIRE(coeffs.at(&x) == 3.);
    }

    {
        LinearExpression le = x * 4.;

        const double constantPart = le.constant();
        const auto & coeffs       = le.coeffs();

        REQUIRE(constantPart == 0.);
        REQUIRE(coeffs.size() == 1);
        REQUIRE(coeffs.at(&x) == 4.);
    }

    {
        LinearExpression le = 3. + x;

        const double constantPart = le.constant();
        const auto & coeffs       = le.coeffs();

        REQUIRE(constantPart == 3.);
        REQUIRE(coeffs.size() == 1);
        REQUIRE(coeffs.at(&x) == 1.);
    }

    {
        LinearExpression le = x + 4.;

        const double constantPart = le.constant();
        const auto & coeffs       = le.coeffs();

        REQUIRE(constantPart == 4.);
        REQUIRE(coeffs.size() == 1);
        REQUIRE(coeffs.at(&x) == 1.);
    }

    {
        LinearExpression le = 3. - x;

        const double constantPart = le.constant();
        const auto & coeffs       = le.coeffs();

        REQUIRE(constantPart == 3.);
        REQUIRE(coeffs.size() == 1);
        REQUIRE(coeffs.at(&x) == -1.);
    }

    {
        LinearExpression le = x - 4.;

        const double constantPart = le.constant();
        const auto & coeffs       = le.coeffs();

        REQUIRE(constantPart == -4.);
        REQUIRE(coeffs.size() == 1);
        REQUIRE(coeffs.at(&x) == 1.);
    }

    {
        LinearExpression le = x + y;

        const double constantPart = le.constant();
        const auto & coeffs       = le.coeffs();

        REQUIRE(constantPart == 0.);
        REQUIRE(coeffs.size() == 2);
        REQUIRE(coeffs.at(&x) == 1.);
        REQUIRE(coeffs.at(&y) == 1.);
    }

    {
        LinearExpression le = x - y;

        const double constantPart = le.constant();
        const auto & coeffs       = le.coeffs();

        REQUIRE(constantPart == 0.);
        REQUIRE(coeffs.size() == 2);
        REQUIRE(coeffs.at(&x) == 1.);
        REQUIRE(coeffs.at(&y) == -1.);
    }

    {
        LinearExpression le = x + y + 1.;

        const double constantPart = le.constant();
        const auto & coeffs       = le.coeffs();

        REQUIRE(constantPart == 1.);
        REQUIRE(coeffs.size() == 2);
        REQUIRE(coeffs.at(&x) == 1.);
        REQUIRE(coeffs.at(&y) == 1.);
    }

    {
        LinearExpression le = 3. * x - 2 * y + 1.;

        const double constantPart = le.constant();
        const auto & coeffs       = le.coeffs();

        REQUIRE(constantPart == 1.);
        REQUIRE(coeffs.size() == 2);
        REQUIRE(coeffs.at(&x) == 3.);
        REQUIRE(coeffs.at(&y) == -2.);
    }

    {
        LinearExpression le = 3.1 * x + 5.6 * y - 1.1 * x + 3 - 1.;

        const double constantPart = le.constant();
        const auto & coeffs       = le.coeffs();

        REQUIRE(constantPart == 2.);
        REQUIRE(coeffs.size() == 2);
        REQUIRE(coeffs.at(&x) == 2.);
        REQUIRE(coeffs.at(&y) == 5.6);
    }

    {
        // the non-const variant
        LinearExpression le1 = x + y - 3;
        LinearExpression le2 = 2 * x - 3. * y;

        LinearExpression le_1plus2 = le1 + le2;

        const double constantPart = le_1plus2.constant();
        const auto & coeffs       = le_1plus2.coeffs();

        REQUIRE(constantPart == -3.);
        REQUIRE(coeffs.size() == 2);
        REQUIRE(coeffs.at(&x) == 3.);
        REQUIRE(coeffs.at(&y) == -2.);
    }

    // the const variant
    {
        const LinearExpression le1 = x + y - 3;
        const LinearExpression le2 = 2 * x - 3. * y;

        LinearExpression le_1plus2 = le1 + le2;

        const double constantPart = le_1plus2.constant();
        const auto & coeffs       = le_1plus2.coeffs();

        REQUIRE(constantPart == -3.);
        REQUIRE(coeffs.size() == 2);
        REQUIRE(coeffs.at(&x) == 3.);
        REQUIRE(coeffs.at(&y) == -2.);
    }

    {
        // note the difference to the next test: here, le is __not__ const!!!
        LinearExpression le = 3. * x - 2.5 * y + 3 - 7.;

        {
            LinearExpression le_scaledBy2Left = 2. * le;

            const double constantPart = le_scaledBy2Left.constant();
            const auto & coeffs       = le_scaledBy2Left.coeffs();

            REQUIRE(constantPart == -8.);
            REQUIRE(coeffs.size() == 2);
            REQUIRE(coeffs.at(&x) == 6.);
            REQUIRE(coeffs.at(&y) == -5.);
        }

        {
            LinearExpression le_scaledBy3Right = le * 3.;

            const double constantPart = le_scaledBy3Right.constant();
            const auto & coeffs       = le_scaledBy3Right.coeffs();

            REQUIRE(constantPart == -12.);
            REQUIRE(coeffs.size() == 2);
            REQUIRE(coeffs.at(&x) == 9.);
            REQUIRE(coeffs.at(&y) == -7.5);
        }
    }

    {
        const LinearExpression le = 3. * x - 2.5 * y + 3 - 7.;

        {
            LinearExpression le_scaledBy2Left = 2. * le;

            const double constantPart = le_scaledBy2Left.constant();
            const auto & coeffs       = le_scaledBy2Left.coeffs();

            REQUIRE(constantPart == -8.);
            REQUIRE(coeffs.size() == 2);
            REQUIRE(coeffs.at(&x) == 6.);
            REQUIRE(coeffs.at(&y) == -5.);
        }

        {
            LinearExpression le_scaledBy3Right = le * 3.;

            const double constantPart = le_scaledBy3Right.constant();
            const auto & coeffs       = le_scaledBy3Right.coeffs();

            REQUIRE(constantPart == -12.);
            REQUIRE(coeffs.size() == 2);
            REQUIRE(coeffs.at(&x) == 9.);
            REQUIRE(coeffs.at(&y) == -7.5);
        }
    }

    {
        LinearExpression le_initiallyEmpty;

        le_initiallyEmpty += x + 1;
        le_initiallyEmpty -= y + 2;

        {
            const double constantPart = le_initiallyEmpty.constant();
            const auto & coeffs       = le_initiallyEmpty.coeffs();

            REQUIRE(constantPart == -1.);
            REQUIRE(coeffs.size() == 2);
            REQUIRE(coeffs.at(&x) == 1.);
            REQUIRE(coeffs.at(&y) == -1.);
        }

        le_initiallyEmpty += 2 * le_initiallyEmpty;

        {
            const double constantPart = le_initiallyEmpty.constant();
            const auto & coeffs       = le_initiallyEmpty.coeffs();

            REQUIRE(constantPart == -3.);
            REQUIRE(coeffs.size() == 2);
            REQUIRE(coeffs.at(&x) == 3.);
            REQUIRE(coeffs.at(&y) == -3.);
        }
    }
}
