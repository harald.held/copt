#include <catch2/catch.hpp>

#include <ExpressionGenerators.h>
#include <MilpModel.h>
#include <ParameterReader.h>
#include <RangeSet.h>

#include <MilpSolver.h>
#include <fmt/core.h>

using namespace copt;

TEST_CASE("a Sudoku puzzle can be solved", "[integrationTest]")
{
    MilpModel sudokuModel;

    // the fixed, already filled fields
    // clang-format off
    std::vector<std::array<int, 3>> G = {
        { 1, 2, 1 }, { 1, 3, 4 }, { 1, 4, 9 }, { 1, 5, 6 }, { 1, 6, 3 },
        { 6, 2, 2 }, { 6, 3, 6 }, { 6, 4, 7 }, { 6, 8, 1 },
        { 7, 1, 6 }, { 7, 3, 8 }, { 7, 5, 9 }, { 7, 6, 2 },
        { 8, 2, 4 }, { 8, 5, 7 }, { 8, 9, 2 },
        { 9, 4, 4 }, { 9, 5, 1 }, { 9, 6, 6 }, { 9, 7, 3 }, { 9, 8, 7 }
    };
    // clang-format on

    constexpr auto m = 3;

    RangeSet I(1, m * m + 1);
    RangeSet J(1, m + 1);

    sudokuModel.addBinaryVariable("x", I, I, I);

    sudokuModel.addConstraint(
        "NB1",
        [&I](const MilpModel &model, int i, int k) {
            return fn::vsum(
                       model, "x",
                       [i, k](int j) -> std::vector<IdxType> {
                           return { i, j, k };
                       },
                       I) == 1;
        },
        I, I);

    sudokuModel.addConstraint(
        "NB2",
        [&I](const MilpModel &model, int j, int k) {
            return fn::vsum(
                       model, "x",
                       [j, k](int i) -> std::vector<IdxType> {
                           return { i, j, k };
                       },
                       I) == 1;
        },
        I, I);

    sudokuModel.addConstraint(
        "NB3",
        [](const MilpModel &model, int k, int p, int q) {
            return fn::vsum(
                       model, "x",
                       [k](int i, int j) -> std::vector<IdxType> {
                           return { i, j, k };
                       },
                       RangeSet(m * p - m + 1, m * p + 1), RangeSet(m * q - m + 1, m * q + 1)) == 1;
        },
        I, J, J);

    sudokuModel.addConstraint(
        "NB4",
        [&I](const MilpModel &model, int i, int j) {
            return fn::vsum(
                       model, "x",
                       [i, j](int k) -> std::vector<IdxType> {
                           return { i, j, k };
                       },
                       I) == 1;
        },
        I, I);

    sudokuModel.addConstraint(
        "NB5",
        [&G](const MilpModel &model, int i, int j, int k) {
            auto it = std::find(std::cbegin(G), std::cend(G), std::array<int, 3>{ i, j, k });

            if (it == std::cend(G))
            {
                return Constraint(Marker::Skip);
            }

            const auto &x_i_j_k = model.V("x", { i, j, k });

            return x_i_j_k == 1.;
        },
        I, I, I);

    sudokuModel.setObjective("obj", [](const MilpModel &model) { return 0.; });

    //    sudokuModel.pprint();

    setenv("COPT_PLUGINS_DIR", "../lib", 1);

    MilpSolver solver(sudokuModel);

    const auto availableSolverNames = solver.availableSolvers();

    REQUIRE(!availableSolverNames.empty());

    for (const auto &solverName : availableSolverNames)
    {
        solver.options().outputLevel = Options::OutputLevel::Verbose;
        solver.solve(solverName);

        // print solution
        for (int i : I)
        {
            for (int j : I)
            {
                for (int k : I)
                {
                    const auto &x = sudokuModel.V("x", { i, j, k });

                    if (x.solutionValue() == Approx(1.))
                    {
                        fmt::print("{} ", k);
                        break;
                    }
                }

                if (j % m == 0)
                {
                    fmt::print("  ");
                }
            }

            if (i % m == 0)
            {
                fmt::print("\n\n");
            }
            else
            {
                fmt::print("\n");
            }
        }
    }
}

TEST_CASE("a Sudoku puzzle can be solved when data is given by parameter file", "[integrationTest]")
{
    MilpModel sudokuModel;

    ParameterReader pr("./resources/sudokuData.yml");

    for (auto param : pr.params())
    {
        sudokuModel.setParameter(std::move(param));
    }

    const int m = static_cast<int>(sudokuModel.P("m").value({}));

    RangeSet I(1, m * m + 1);
    RangeSet J(1, m + 1);

    sudokuModel.addBinaryVariable("x", I, I, I);

    sudokuModel.addConstraint(
        "NB1",
        [&I](const MilpModel &model, int i, int k) {
            return fn::vsum(
                       model, "x",
                       [i, k](int j) -> std::vector<IdxType> {
                           return { i, j, k };
                       },
                       I) == 1;
        },
        I, I);

    sudokuModel.addConstraint(
        "NB2",
        [&I](const MilpModel &model, int j, int k) {
            return fn::vsum(
                       model, "x",
                       [j, k](int i) -> std::vector<IdxType> {
                           return { i, j, k };
                       },
                       I) == 1;
        },
        I, I);

    sudokuModel.addConstraint(
        "NB3",
        [](const MilpModel &model, int k, int p, int q) {
            const auto m = static_cast<int>(model.P("m").value({}));

            return fn::vsum(
                       model, "x",
                       [k](int i, int j) -> std::vector<IdxType> {
                           return { i, j, k };
                       },
                       RangeSet(m * p - m + 1, m * p + 1), RangeSet(m * q - m + 1, m * q + 1)) == 1;
        },
        I, J, J);

    sudokuModel.addConstraint(
        "NB4",
        [&I](const MilpModel &model, int i, int j) {
            return fn::vsum(
                       model, "x",
                       [i, j](int k) -> std::vector<IdxType> {
                           return { i, j, k };
                       },
                       I) == 1;
        },
        I, I);

    sudokuModel.addConstraint(
        "NB5",
        [](const MilpModel &model, int i, int j, int k) {
            const auto G = model.P("G");

            if (!G.hasIndex({ i, j }))
            {
                return Constraint(Marker::Skip);
            }

            const auto G_i_j = G.value({ i, j });

            if (G_i_j == k)
            {
                const auto &x_i_j_k = model.V("x", { i, j, k });

                return x_i_j_k == 1.;
            }

            return Constraint(Marker::Skip);
        },
        I, I, I);

    sudokuModel.setObjective("obj", [](const MilpModel &model) { return 0.; });

    //    sudokuModel.pprint();

    MilpSolver solver(sudokuModel);

    const auto availableSolverNames = solver.availableSolvers();

    REQUIRE(!availableSolverNames.empty());

    for (const auto &solverName : availableSolverNames)
    {
        solver.options().outputLevel = Options::OutputLevel::Verbose;
        solver.solve(solverName);

        // print solution
        for (int i : I)
        {
            for (int j : I)
            {
                for (int k : I)
                {
                    const auto &x = sudokuModel.V("x", { i, j, k });

                    if (x.solutionValue() == Approx(1.))
                    {
                        fmt::print("{} ", k);
                        break;
                    }
                }

                if (j % m == 0)
                {
                    fmt::print("  ");
                }
            }

            if (i % m == 0)
            {
                fmt::print("\n\n");
            }
            else
            {
                fmt::print("\n");
            }
        }
    }
}
