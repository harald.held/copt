#include <catch2/catch.hpp>

#include <FilteredIndexSet.h>
#include <RangeSet.h>

using namespace copt;

SCENARIO("a new RangeSet can be created, has the typical properties, and can be iterated over", "[RangeSet]")
{
    GIVEN("a RangeSet from 5 to 10 (excluded)")
    {
        RangeSet I(5, 10);

        THEN("the RangeSet contains 5 elements")
        {
            REQUIRE(I.count() == 5);
        }

        THEN("the RangeSet can be iterated over")
        {
            int expected = 5;

            for (const auto &i : I)
            {
                REQUIRE(i == expected++);
            }
        }

        THEN("std algorithms like minmax_element work with the RangeSet")
        {
            const auto [min_it, max_it] = std::minmax_element(std::begin(I), std::end(I));

            REQUIRE(*min_it == 5);
            REQUIRE(*max_it == 9);
        }

        THEN("std algorithms that require forward iterators like std::copy work with RangeSet")
        {
            std::vector<int> v;
            std::copy(std::begin(I), std::end(I), std::back_inserter(v));

            for (int i = 5; i < 10; ++i)
            {
                REQUIRE(i == v[i - 5]);
            }
        }
    }

    GIVEN("a RangeSet with an empty range, e.g. from 1 to 1")
    {
        RangeSet I(1, 1);

        THEN("the RangeSet is empty")
        {
            REQUIRE(I.count() == 0);
            REQUIRE(I.empty());
        }
    }

    GIVEN("a RangeSet with an invalid range, e.g. from 5 to 3")
    {
        RangeSet I(5, 3);

        THEN("the RangeSet is empty")
        {
            REQUIRE(I.count() == 0);
            REQUIRE(I.empty());
        }
    }
}

SCENARIO("a RangeSet can be filtered by specifying a predicate", "[RangeSet]")
{
    GIVEN("a RangeSet from 0 to 10 (excluded)")
    {
        RangeSet I(0, 10);

        THEN("that RangeSet contains 10 elements")
        {
            REQUIRE(I.count() == 10);
        }

        WHEN("filtering even numbers from that RangeSet via the decorator FilteredIndexSet")
        {
            FilteredIndexSet I_even(&I, [](const int &i) { return i % 2 == 0; });

            THEN("the filtered IndexSet has 5 elements")
            {
                REQUIRE(I_even.count() == 5);
            }

            THEN("the filtered IndexSet is not empty")
            {
                REQUIRE(!I_even.empty());
            }

            THEN("the IndexSet contains {0, 2, 4, 6, 8}")
            {
                std::vector<int> indices;

                for (auto i : I_even)
                {
                    indices.push_back(i);
                }

                REQUIRE(indices == std::vector<int>{ 0, 2, 4, 6, 8 });
            }
        }

        WHEN("filtering odd numbers from that RangeSet via the decorator FilteredIndexSet")
        {
            FilteredIndexSet I_odd(&I, [](const int &i) { return i % 2 == 1; });

            THEN("the filtered IndexSet has 5 elements")
            {
                REQUIRE(I_odd.count() == 5);
            }

            THEN("the filtered IndexSet is not empty")
            {
                REQUIRE(!I_odd.empty());
            }

            THEN("the IndexSet contains {1, 3, 5, 7, 9}")
            {
                std::vector<int> indices;

                for (auto i : I_odd)
                {
                    indices.push_back(i);
                }

                REQUIRE(indices == std::vector<int>{ 1, 3, 5, 7, 9 });
            }
        }

        WHEN("filtering even and odd numbers from that RangeSet via the decorator FilteredIndexSet")
        {
            FilteredIndexSet I_odd(&I, [](const int &i) { return i % 2 == 1; });
            FilteredIndexSet I_oddAndEven(&I_odd, [](const int &i) { return i % 2 == 0; });

            THEN("the filtered IndexSet has 0 elements")
            {
                REQUIRE(I_oddAndEven.count() == 0);
            }

            THEN("the filtered IndexSet is empty")
            {
                REQUIRE(I_oddAndEven.empty());
            }

            THEN("the IndexSet contains no element, i.e. iterating over it does nothing")
            {
                std::vector<int> indices;

                for (auto i : I_oddAndEven)
                {
                    indices.push_back(i);
                }

                REQUIRE(indices.empty());
            }
        }
    }
}

SCENARIO("a RangeSet can be created by giving only an upper bound", "[RangeSet]")
{
    GIVEN("a RangeSet with only an upper bound of 10")
    {
        RangeSet I(10);

        THEN("the RangeSet has 10 elements")
        {
            REQUIRE(I.count() == 10);

            AND_THEN("the RangeSet contains elements {0, 1, ..., 9")
            {
                std::vector<int> indices;

                for (auto i : I)
                {
                    indices.push_back(i);
                }

                REQUIRE(indices == std::vector<int>{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
            }
        }
    }
}
