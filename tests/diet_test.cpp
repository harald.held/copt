#include <catch2/catch.hpp>

#include <ExpressionGenerators.h>
#include <MilpModel.h>
#include <MilpSolver.h>
#include <ParameterReader.h>
#include <StringSet.h>

#include <map>
#include <optional>

using namespace copt;

TEST_CASE("the classical diet MILP can be solved", "[integrationTest]")
{
    // cf. https://github.com/Pyomo/pyomo/blob/master/examples/pyomo/amplbook2/diet.mod
    // and https://github.com/Pyomo/pyomo/blob/master/examples/pyomo/amplbook2/diet.dat

    // data ############################################################################################################
    const StringSet NUTR({ "A", "B1", "B2", "C" });
    const StringSet FOOD({ "BEEF", "CHK", "FISH", "HAM", "MCH", "MTL", "SPG", "TUR" });

    const std::map<std::string, std::optional<double>> f_min = {
        { "BEEF", 0. }, { "CHK", 0. }, { "FISH", 0. }, { "HAM", 0. },
        { "MCH", 0. },  { "MTL", 0. }, { "SPG", 0. },  { "TUR", 0. },
    };

    const std::map<std::string, std::optional<double>> f_max = {
        { "BEEF", 100. }, { "CHK", 100. }, { "FISH", 100. }, { "HAM", 100. },
        { "MCH", 100. },  { "MTL", 100. }, { "SPG", 100. },  { "TUR", 100. },
    };

    const std::map<std::string, double> cost = {
        { "BEEF", 3.19 }, { "CHK", 2.59 }, { "FISH", 2.29 }, { "HAM", 2.89 },
        { "MCH", 1.89 },  { "MTL", 1.99 }, { "SPG", 1.99 },  { "TUR", 2.49 },
    };

    const std::map<std::string, double> n_min = {
        { "A", 700. },
        { "C", 700. },
        { "B1", 700. },
        { "B2", 700. },
    };

    const std::map<std::string, double> n_max = {
        { "A", 10000. },
        { "C", 10000. },
        { "B1", 10000. },
        { "B2", 10000. },
    };

    const std::map<std::string, std::map<std::string, double>> amt = {
        // clang-format off
        {"BEEF", {{ "A", 60. }, { "C", 20. }, { "B1", 10. }, { "B2", 15. }}},
        {"CHK",  {{ "A",  8. }, { "C",  0. }, { "B1", 20. }, { "B2", 20. }}},
        {"FISH", {{ "A",  8. }, { "C", 10. }, { "B1", 15. }, { "B2", 10. }}},
        {"HAM",  {{ "A", 40. }, { "C", 40. }, { "B1", 35. }, { "B2", 10. }}},
        {"MCH",  {{ "A", 15. }, { "C", 35. }, { "B1", 15. }, { "B2", 15. }}},
        {"MTL",  {{ "A", 70. }, { "C", 30. }, { "B1", 15. }, { "B2", 15. }}},
        {"SPG",  {{ "A", 25. }, { "C", 50. }, { "B1", 25. }, { "B2", 15. }}},
        {"TUR",  {{ "A", 60. }, { "C", 20. }, { "B1", 15. }, { "B2", 10. }}},
        // clang-format on
    };

    // model ###########################################################################################################
    MilpModel dietModel;

    dietModel.addVariable(
        "buy", [&f_min, &f_max](const std::string &food) { return std::make_tuple(f_min.at(food), f_max.at(food)); },
        VariableType::Continuous, FOOD);

    dietModel.setObjective("total cost", [&cost, &FOOD](const MilpModel &model) {
        LinearExpression le;

        for (const auto &food : FOOD)
        {
            const auto &buy = model.V("buy", { food });

            le += cost.at(food) * buy;
        }

        return le;
    });

    dietModel.addConstraint(
        "diet lb",
        [&n_min, &FOOD, &amt](const MilpModel &model, const std::string &i) {
            LinearExpression le;

            for (const auto &j : FOOD)
            {
                const auto &buy = model.V("buy", { j });

                le += amt.at(j).at(i) * buy;
            }

            return n_min.at(i) <= le;
        },
        NUTR);

    dietModel.addConstraint(
        "diet ub",
        [&n_max, &FOOD, &amt](const MilpModel &model, const std::string &i) {
            LinearExpression le;

            for (const auto &j : FOOD)
            {
                const auto &buy = model.V("buy", { j });

                le += amt.at(j).at(i) * buy;
            }

            return le <= n_max.at(i);
        },
        NUTR);

    //    dietModel.pprint();

    // solution ########################################################################################################
    MilpSolver solver(dietModel);

    const auto availableSolverNames = solver.availableSolvers();

    REQUIRE(!availableSolverNames.empty());

    for (const auto &solverName : availableSolverNames)
    {
        CAPTURE(solverName);

        solver.options().outputLevel = Options::OutputLevel::None;
        solver.solve(solverName);

        // print solution
        for (const auto &i : FOOD)
        {
            CAPTURE(i);

            const auto &buy = dietModel.V("buy", { i });

            if (i == "MCH")
            {
                REQUIRE(*dietModel.V("buy", { i }).solutionValue() == Approx(46.6666666666));
            }
            else
            {
                REQUIRE(*dietModel.V("buy", { i }).solutionValue() == Approx(0.).margin(1.E-14));
            }
        }
    }
}

TEST_CASE("the same classical diet MILP can be solved, but data is passed via model parameters", "[integrationTest]")
{
    // cf. https://github.com/Pyomo/pyomo/blob/master/examples/pyomo/amplbook2/diet.mod
    // and https://github.com/Pyomo/pyomo/blob/master/examples/pyomo/amplbook2/diet.dat

    // data ############################################################################################################
    const StringSet NUTR({ "A", "B1", "B2", "C" });
    const StringSet FOOD({ "BEEF", "CHK", "FISH", "HAM", "MCH", "MTL", "SPG", "TUR" });

    Parameter f_min("f_min");
    Parameter f_max("f_max");
    Parameter cost("cost");
    Parameter n_min("n_min");
    Parameter n_max("n_max");
    Parameter amt("amt");

    for (const auto &food : FOOD)
    {
        f_min.setValue({ food }, 0.);
        f_max.setValue({ food }, 100.);
    }

    for (const auto &c : std::vector<std::pair<std::string, double>>{
             { "BEEF", 3.19 },
             { "CHK", 2.59 },
             { "FISH", 2.29 },
             { "HAM", 2.89 },
             { "MCH", 1.89 },
             { "MTL", 1.99 },
             { "SPG", 1.99 },
             { "TUR", 2.49 },
         })
    {
        cost.setValue({ c.first }, c.second);
    }

    for (const auto &nutr : NUTR)
    {
        n_min.setValue({ nutr }, 700.);
        n_max.setValue({ nutr }, 10000.);
    }

    for (const auto &a : std::map<std::string, std::map<std::string, double>>{
             // clang-format off
        {"BEEF", {{ "A", 60. }, { "C", 20. }, { "B1", 10. }, { "B2", 15. }}},
        {"CHK",  {{ "A",  8. }, { "C",  0. }, { "B1", 20. }, { "B2", 20. }}},
        {"FISH", {{ "A",  8. }, { "C", 10. }, { "B1", 15. }, { "B2", 10. }}},
        {"HAM",  {{ "A", 40. }, { "C", 40. }, { "B1", 35. }, { "B2", 10. }}},
        {"MCH",  {{ "A", 15. }, { "C", 35. }, { "B1", 15. }, { "B2", 15. }}},
        {"MTL",  {{ "A", 70. }, { "C", 30. }, { "B1", 15. }, { "B2", 15. }}},
        {"SPG",  {{ "A", 25. }, { "C", 50. }, { "B1", 25. }, { "B2", 15. }}},
        {"TUR",  {{ "A", 60. }, { "C", 20. }, { "B1", 15. }, { "B2", 10. }}},
             // clang-format on
         })
    {
        const auto &food = a.first;

        for (const auto &nutr : a.second)
        {
            amt.setValue({ food, nutr.first }, nutr.second);
        }
    }

    // model ###########################################################################################################
    MilpModel dietModel;

    dietModel.setParameter(std::move(f_min));
    dietModel.setParameter(std::move(f_max));
    dietModel.setParameter(std::move(cost));
    dietModel.setParameter(std::move(n_min));
    dietModel.setParameter(std::move(n_max));
    dietModel.setParameter(std::move(amt));

    dietModel.addVariable(
        "buy",
        [&model = dietModel](const std::string &food) -> MilpModel::BoundType {
            const auto &f_min = model.P("f_min");
            const auto &f_max = model.P("f_max");
            return std::make_tuple(f_min.value({ food }), f_max.value({ food }));
        },
        VariableType::Continuous, FOOD);

    dietModel.setObjective("total cost", [&FOOD](const MilpModel &model) {
        LinearExpression le;

        const auto &cost = model.P("cost");

        for (const auto &food : FOOD)
        {
            const auto &buy = model.V("buy", { food });

            le += cost.value({ food }) * buy;
        }

        return le;
    });

    dietModel.addConstraint(
        "diet lb",
        [&FOOD](const MilpModel &model, const std::string &i) {
            LinearExpression le;

            const auto &n_min = model.P("n_min");
            const auto &amt   = model.P("amt");

            for (const auto &j : FOOD)
            {
                const auto &buy = model.V("buy", { j });

                le += amt.value({ j, i }) * buy;
            }

            return n_min.value({ i }) <= le;
        },
        NUTR);

    dietModel.addConstraint(
        "diet ub",
        [&FOOD](const MilpModel &model, const std::string &i) {
            LinearExpression le;

            const auto &n_max = model.P("n_max");
            const auto &amt   = model.P("amt");

            for (const auto &j : FOOD)
            {
                const auto &buy = model.V("buy", { j });

                le += amt.value({ j, i }) * buy;
            }

            return le <= n_max.value({ i });
        },
        NUTR);

    //    dietModel.pprint();

    // solution ########################################################################################################
    MilpSolver solver(dietModel);

    const auto availableSolverNames = solver.availableSolvers();

    REQUIRE(!availableSolverNames.empty());

    for (const auto &solverName : availableSolverNames)
    {
        CAPTURE(solverName);

        solver.options().outputLevel = Options::OutputLevel::None;
        solver.solve(solverName);

        // print solution
        for (const auto &i : FOOD)
        {
            CAPTURE(i);

            const auto &buy = dietModel.V("buy", { i });

            if (i == "MCH")
            {
                REQUIRE(*dietModel.V("buy", { i }).solutionValue() == Approx(46.6666666666));
            }
            else
            {
                REQUIRE(*dietModel.V("buy", { i }).solutionValue() == Approx(0.).margin(1.E-14));
            }
        }
    }
}

TEST_CASE("the same classical diet MILP can be solved, but data is passed via model parameters and the dot product "
          "expression generator is being used",
          "[integrationTest]")
{
    // cf. https://github.com/Pyomo/pyomo/blob/master/examples/pyomo/amplbook2/diet.mod
    // and https://github.com/Pyomo/pyomo/blob/master/examples/pyomo/amplbook2/diet.dat

    // data ############################################################################################################
    const StringSet NUTR({ "A", "B1", "B2", "C" });
    const StringSet FOOD({ "BEEF", "CHK", "FISH", "HAM", "MCH", "MTL", "SPG", "TUR" });

    Parameter f_min("f_min");
    Parameter f_max("f_max");
    Parameter cost("cost");
    Parameter n_min("n_min");
    Parameter n_max("n_max");
    Parameter amt("amt");

    for (const auto &food : FOOD)
    {
        f_min.setValue({ food }, 0.);
        f_max.setValue({ food }, 100.);
    }

    for (const auto &c : std::vector<std::pair<std::string, double>>{
             { "BEEF", 3.19 },
             { "CHK", 2.59 },
             { "FISH", 2.29 },
             { "HAM", 2.89 },
             { "MCH", 1.89 },
             { "MTL", 1.99 },
             { "SPG", 1.99 },
             { "TUR", 2.49 },
         })
    {
        cost.setValue({ c.first }, c.second);
    }

    for (const auto &nutr : NUTR)
    {
        n_min.setValue({ nutr }, 700.);
        n_max.setValue({ nutr }, 10000.);
    }

    for (const auto &a : std::map<std::string, std::map<std::string, double>>{
             // clang-format off
        {"BEEF", {{ "A", 60. }, { "C", 20. }, { "B1", 10. }, { "B2", 15. }}},
        {"CHK",  {{ "A",  8. }, { "C",  0. }, { "B1", 20. }, { "B2", 20. }}},
        {"FISH", {{ "A",  8. }, { "C", 10. }, { "B1", 15. }, { "B2", 10. }}},
        {"HAM",  {{ "A", 40. }, { "C", 40. }, { "B1", 35. }, { "B2", 10. }}},
        {"MCH",  {{ "A", 15. }, { "C", 35. }, { "B1", 15. }, { "B2", 15. }}},
        {"MTL",  {{ "A", 70. }, { "C", 30. }, { "B1", 15. }, { "B2", 15. }}},
        {"SPG",  {{ "A", 25. }, { "C", 50. }, { "B1", 25. }, { "B2", 15. }}},
        {"TUR",  {{ "A", 60. }, { "C", 20. }, { "B1", 15. }, { "B2", 10. }}},
             // clang-format on
         })
    {
        const auto &food = a.first;

        for (const auto &nutr : a.second)
        {
            amt.setValue({ food, nutr.first }, nutr.second);
        }
    }

    // model ###########################################################################################################
    MilpModel dietModel;

    dietModel.setParameter(std::move(f_min));
    dietModel.setParameter(std::move(f_max));
    dietModel.setParameter(std::move(cost));
    dietModel.setParameter(std::move(n_min));
    dietModel.setParameter(std::move(n_max));
    dietModel.setParameter(std::move(amt));

    dietModel.addVariable(
        "buy",
        [&model = dietModel](const std::string &food) -> MilpModel::BoundType {
            const auto &f_min = model.P("f_min");
            const auto &f_max = model.P("f_max");
            return std::make_tuple(f_min.value({ food }), f_max.value({ food }));
        },
        VariableType::Continuous, FOOD);

    dietModel.setObjective("total cost",
                           [&FOOD](const MilpModel &model) { return fn::dot(model, "cost", "buy", FOOD); });

    dietModel.addConstraint(
        "diet lb",
        [&FOOD](const MilpModel &model, const std::string &i) {
            LinearExpression le;

            const auto &n_min = model.P("n_min");
            const auto &amt   = model.P("amt");

            for (const auto &j : FOOD)
            {
                const auto &buy = model.V("buy", { j });

                le += amt.value({ j, i }) * buy;
            }

            return n_min.value({ i }) <= le;
        },
        NUTR);

    dietModel.addConstraint(
        "diet ub",
        [&FOOD](const MilpModel &model, const std::string &i) {
            LinearExpression le;

            const auto &n_max = model.P("n_max");
            const auto &amt   = model.P("amt");

            for (const auto &j : FOOD)
            {
                const auto &buy = model.V("buy", { j });

                le += amt.value({ j, i }) * buy;
            }

            return le <= n_max.value({ i });
        },
        NUTR);

    //    dietModel.pprint();

    // solution ########################################################################################################
    MilpSolver solver(dietModel);

    const auto availableSolverNames = solver.availableSolvers();

    REQUIRE(!availableSolverNames.empty());

    for (const auto &solverName : availableSolverNames)
    {
        CAPTURE(solverName);

        solver.options().outputLevel = Options::OutputLevel::None;
        solver.solve(solverName);

        // print solution
        for (const auto &i : FOOD)
        {
            CAPTURE(i);

            const auto &buy = dietModel.V("buy", { i });

            if (i == "MCH")
            {
                REQUIRE(*dietModel.V("buy", { i }).solutionValue() == Approx(46.6666666666));
            }
            else
            {
                REQUIRE(*dietModel.V("buy", { i }).solutionValue() == Approx(0.).margin(1.E-14));
            }
        }
    }
}

TEST_CASE("the same classical diet MILP can be solved, but data is passed via model parameters read from a YAML file, "
          "and the dot product expression generator is being used",
          "[integrationTest]")
{
    // cf. https://github.com/Pyomo/pyomo/blob/master/examples/pyomo/amplbook2/diet.mod
    // and https://github.com/Pyomo/pyomo/blob/master/examples/pyomo/amplbook2/diet.dat

    // data ############################################################################################################
    const StringSet NUTR({ "A", "B1", "B2", "C" });
    const StringSet FOOD({ "BEEF", "CHK", "FISH", "HAM", "MCH", "MTL", "SPG", "TUR" });

    ParameterReader pr("./resources/dietData.yml");

    // model ###########################################################################################################
    MilpModel dietModel;

    for (auto param : pr.params())
    {
        dietModel.setParameter(std::move(param));
    }

    dietModel.addVariable(
        "buy",
        [&model = dietModel](const std::string &food) -> MilpModel::BoundType {
            const auto &f_min = model.P("f_min");
            const auto &f_max = model.P("f_max");
            return std::make_tuple(f_min.value({ food }), f_max.value({ food }));
        },
        VariableType::Continuous, FOOD);

    dietModel.setObjective("total cost",
                           [&FOOD](const MilpModel &model) { return fn::dot(model, "cost", "buy", FOOD); });

    dietModel.addConstraint(
        "diet lb",
        [&FOOD](const MilpModel &model, const std::string &i) {
            LinearExpression le;

            const auto &n_min = model.P("n_min");

            return n_min.value({ i }) <=
                   fn::dotMap(
                       model, "amt", "buy",
                       [nutr = i](const IdxType &food) {
                           return std::make_tuple(std::vector<IdxType>{ food, nutr }, std::vector<IdxType>{ food });
                       },
                       FOOD);
        },
        NUTR);

    dietModel.addConstraint(
        "diet ub",
        [&FOOD](const MilpModel &model, const std::string &i) {
            LinearExpression le;

            const auto &n_max = model.P("n_max");

            return fn::dotMap(
                       model, "amt", "buy",
                       [nutr = i](const IdxType &food) {
                           return std::make_tuple(std::vector<IdxType>{ food, nutr }, std::vector<IdxType>{ food });
                       },
                       FOOD) <= n_max.value({ i });
        },
        NUTR);

    //    dietModel.pprint();

    // solution ########################################################################################################
    MilpSolver solver(dietModel);

    const auto availableSolverNames = solver.availableSolvers();

    REQUIRE(!availableSolverNames.empty());

    for (const auto &solverName : availableSolverNames)
    {
        CAPTURE(solverName);

        solver.options().outputLevel = Options::OutputLevel::None;
        solver.solve(solverName);

        // print solution
        for (const auto &i : FOOD)
        {
            CAPTURE(i);

            const auto &buy = dietModel.V("buy", { i });

            if (i == "MCH")
            {
                REQUIRE(*dietModel.V("buy", { i }).solutionValue() == Approx(46.6666666666));
            }
            else
            {
                REQUIRE(*dietModel.V("buy", { i }).solutionValue() == Approx(0.).margin(1.E-14));
            }
        }
    }
}
