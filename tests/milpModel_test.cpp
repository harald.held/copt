#include <catch2/catch.hpp>

#include <CartesianProduct.h>
#include <Constraint.h>
#include <MilpModel.h>
#include <RangeSet.h>
#include <StringSet.h>

#include <fmt/core.h>

using namespace copt;

SCENARIO("variables can be added to a MilpModel", "[milpModel]")
{
    GIVEN("a new MilpModel instance")
    {
        MilpModel milpModel;

        WHEN("adding a new variable to the model")
        {
            milpModel.addContinuousUnboundedVariable("x");

            THEN("the model has 1 variable")
            {
                REQUIRE(milpModel.numVars() == 1);
            }

            AND_WHEN("querying the model for a variable named 'x' w/o any index")
            {
                const auto &v = milpModel.V("x", {});

                THEN("that variable is named 'x'")
                {
                    REQUIRE(v.name() == "x");
                }
                THEN("that variable is of type Continuous")
                {
                    REQUIRE(v.varType() == VariableType::Continuous);
                }
                THEN("that variable is unbounded")
                {
                    REQUIRE(v.lb() == std::numeric_limits<double>::lowest());
                    REQUIRE(v.ub() == std::numeric_limits<double>::max());
                }
            }

            AND_WHEN("querying the model for a variable named 'x' by just its name")
            {
                const auto &v = milpModel.V("x");

                THEN("that variable is named 'x'")
                {
                    REQUIRE(v.name() == "x");
                }
                THEN("that variable is of type Continuous")
                {
                    REQUIRE(v.varType() == VariableType::Continuous);
                }
                THEN("that variable is unbounded")
                {
                    REQUIRE(v.lb() == std::numeric_limits<double>::lowest());
                    REQUIRE(v.ub() == std::numeric_limits<double>::max());
                }
            }

            THEN("querying the model for a variable named 'y' w/o any index throws an exception")
            {
                REQUIRE_THROWS(milpModel.V("y", {}));
                REQUIRE_THROWS(milpModel.V("y"));
            }

            THEN("querying the model for a variable named 'x' w/ an index throws an exception")
            {
                REQUIRE_THROWS(milpModel.V("x", { 0 }));
            }
        }

        AND_GIVEN("a RangeSet with indices between 5 and 10 (incl.)")
        {
            RangeSet I(5, 11);

            WHEN("adding a variable indexed by the RangeSet to the model")
            {
                milpModel.addContinuousUnboundedVariable("x", I);

                THEN("the model has 6 variables")
                {
                    REQUIRE(milpModel.numVars() == 6);
                }

                WHEN("querying the model for a variable named 'x' w/o any index throws an exception")
                {
                    REQUIRE_THROWS(milpModel.V("x", {}));
                    REQUIRE_THROWS(milpModel.V("x"));
                }

                for (auto i : I)
                {
                    WHEN(fmt::format("querying the model for a variable named 'x' w/ index {}", i))
                    {
                        const auto &v = milpModel.V("x", { i });

                        THEN(fmt::format("that variable's name is 'x_{}'", i))
                        {
                            REQUIRE(v.name() == fmt::format("x_{}", i));
                        }
                    }
                }
            }

            WHEN("adding a variable with 2 indices, both indexed by the RangeSet to the model")
            {
                milpModel.addContinuousUnboundedVariable("x", I, I);

                THEN("the model has 36 variables")
                {
                    REQUIRE(milpModel.numVars() == 36);
                }

                WHEN("querying the model for a variable named 'x' w/o any index throws an exception")
                {
                    REQUIRE_THROWS(milpModel.V("x", {}));
                    REQUIRE_THROWS(milpModel.V("x"));
                }

                for (auto i : I)
                {
                    for (auto j : I)
                    {
                        WHEN(fmt::format("querying the model for a variable named 'x' w/ index ({}, {})", i, j))
                        {
                            const auto &v = milpModel.V("x", { i, j });

                            THEN(fmt::format("that variable's name is 'x_{}_{}'", i, j))
                            {
                                REQUIRE(v.name() == fmt::format("x_{}_{}", i, j));
                            }
                        }
                    }
                }

                WHEN("querying the model for all variables of name 'x'")
                {
                    const auto vx = milpModel.Vs("x");

                    THEN("we get a vector of 36 variables")
                    {
                        REQUIRE(vx.size() == 36);
                    }
                }
            }
        }

        AND_GIVEN("a StringSet with indices 'a', 'bb', 'ccc'")
        {
            StringSet I({ "a", "bb", "ccc" });

            WHEN("adding a variable indexed by the StringSet to the model")
            {
                milpModel.addContinuousUnboundedVariable("x", I);

                THEN("the model has 3 variables")
                {
                    REQUIRE(milpModel.numVars() == 3);
                }

                WHEN("querying the model for a variable named 'x' w/o any index throws an exception")
                {
                    REQUIRE_THROWS(milpModel.V("x", {}));
                    REQUIRE_THROWS(milpModel.V("x"));
                }

                for (const auto &i : I)
                {
                    WHEN(fmt::format("querying the model for a variable named 'x' w/ index {}", i))
                    {
                        const auto &v = milpModel.V("x", { i });

                        THEN(fmt::format("that variable's name is 'x_{}'", i))
                        {
                            REQUIRE(v.name() == fmt::format("x_{}", i));
                        }
                    }
                }
            }

            WHEN("adding a variable with 2 indices, both indexed by the StringSet to the model")
            {
                milpModel.addContinuousUnboundedVariable("x", I, I);

                THEN("the model has 9 variables")
                {
                    REQUIRE(milpModel.numVars() == 9);
                }

                WHEN("querying the model for a variable named 'x' w/o any index throws an exception")
                {
                    REQUIRE_THROWS(milpModel.V("x", {}));
                    REQUIRE_THROWS(milpModel.V("x"));
                }

                for (auto i : I)
                {
                    for (auto j : I)
                    {
                        WHEN(fmt::format("querying the model for a variable named 'x' w/ index ({}, {})", i, j))
                        {
                            const auto &v = milpModel.V("x", { i, j });

                            THEN(fmt::format("that variable's name is 'x_{}_{}'", i, j))
                            {
                                REQUIRE(v.name() == fmt::format("x_{}_{}", i, j));
                            }
                        }
                    }
                }

                WHEN("querying the model for all variables of name 'x'")
                {
                    const auto vx = milpModel.Vs("x");

                    THEN("we get a vector of 9 variables")
                    {
                        REQUIRE(vx.size() == 9);
                    }
                }
            }

            WHEN("adding a variable with 2 indices, one indexed by a StringSet and one by a RangeSet to the model")
            {
                RangeSet J(5);
                milpModel.addContinuousUnboundedVariable("x", I, J);

                THEN("the model has 15 variables")
                {
                    REQUIRE(milpModel.numVars() == 15);
                }

                WHEN("querying the model for a variable named 'x' w/o any index throws an exception")
                {
                    REQUIRE_THROWS(milpModel.V("x", {}));
                    REQUIRE_THROWS(milpModel.V("x"));
                }

                for (auto i : I)
                {
                    for (auto j : J)
                    {
                        WHEN(fmt::format("querying the model for a variable named 'x' w/ index ({}, {})", i, j))
                        {
                            const auto &v = milpModel.V("x", { i, j });

                            THEN(fmt::format("that variable's name is 'x_{}_{}'", i, j))
                            {
                                REQUIRE(v.name() == fmt::format("x_{}_{}", i, j));
                            }
                        }
                    }
                }

                WHEN("querying the model for all variables of name 'x'")
                {
                    const auto vx = milpModel.Vs("x");

                    THEN("we get a vector of 15 variables")
                    {
                        REQUIRE(vx.size() == 15);
                    }
                }
            }
        }

        WHEN("adding a variable to the MilpModel")
        {
            milpModel.addContinuousUnboundedVariable("x");

            THEN("adding another variable with the same name throws an exception")
            {
                RangeSet I(5, 11);

                REQUIRE_THROWS(milpModel.addContinuousUnboundedVariable("x"));
                REQUIRE_THROWS(milpModel.addContinuousUnboundedVariable("x", I));
            }
        }
    }
}

SCENARIO("constraints with an upper bound can be added to a MilpModel", "[milpModel]")
{
    GIVEN("a MilpModel instance with two variables x and y")
    {
        MilpModel milpModel;

        milpModel.addContinuousUnboundedVariable("x");
        milpModel.addContinuousUnboundedVariable("y");

        THEN("the model knows 2 variable names")
        {
            const auto allVarNamesInModel = milpModel.varNames();

            REQUIRE(allVarNamesInModel.size() == 2);
            REQUIRE(allVarNamesInModel == std::vector<std::string>({ "x", "y" }));
        }

        WHEN("adding a new constraint to the model with upper bound as RHS")
        {
            constexpr auto constraintName = "cstr 1";

            milpModel.addConstraint(constraintName, [](const MilpModel &model) {
                const auto &x = model.V("x");
                const auto &y = model.V("y");

                return x + 3 * y <= 10.;
            });

            THEN("the model has 1 constraint")
            {
                REQUIRE(milpModel.numConstraints() == 1);
            }

            THEN("the model has the constraint we just added")
            {
                const auto &constraint = milpModel.C(constraintName);

                REQUIRE(constraint.upperBound() == 10.);
                REQUIRE(constraint.lowerBound() == std::numeric_limits<double>::lowest());
                REQUIRE(constraint.name() == constraintName);

                const auto &le = constraint.linearExpression();

                const double constantPart = le.constant();
                const auto & varCoeffs    = le.coeffs();

                REQUIRE(constantPart == 0.);
                REQUIRE(varCoeffs.size() == 2);

                const auto &x = milpModel.V("x");
                const auto &y = milpModel.V("y");

                REQUIRE(varCoeffs.at(&x) == 1.);
                REQUIRE(varCoeffs.at(&y) == 3.);
            }

            AND_WHEN("adding another constraint to the model")
            {
                constexpr auto constraintName2 = "cstr 2";

                milpModel.addConstraint(constraintName2, [](const MilpModel &model) {
                    const auto &x = model.V("x");
                    const auto &y = model.V("y");

                    return x + 3 * y + 1.2 <= 10.;
                });

                THEN("the model has 2 constraint")
                {
                    REQUIRE(milpModel.numConstraints() == 2);
                }

                THEN("the model has the constraint we just added")
                {
                    const auto &constraint = milpModel.C(constraintName2);

                    REQUIRE(constraint.upperBound() == 8.8);
                    REQUIRE(constraint.lowerBound() == std::numeric_limits<double>::lowest());
                    REQUIRE(constraint.name() == constraintName2);

                    const auto &le = constraint.linearExpression();

                    const double constantPart = le.constant();
                    const auto & varCoeffs    = le.coeffs();

                    REQUIRE(constantPart == 0.);
                    REQUIRE(varCoeffs.size() == 2);

                    const auto &x = milpModel.V("x");
                    const auto &y = milpModel.V("y");

                    REQUIRE(varCoeffs.at(&x) == 1.);
                    REQUIRE(varCoeffs.at(&y) == 3.);
                }
            }
        }

        WHEN("adding a new constraint to the model with upper bound as LHS")
        {
            constexpr auto constraintName = "cstr 1";

            milpModel.addConstraint(constraintName, [](const MilpModel &model) {
                const auto &x = model.V("x");
                const auto &y = model.V("y");

                return 10. >= x + 3 * y;
            });

            THEN("the model has 1 constraint")
            {
                REQUIRE(milpModel.numConstraints() == 1);
            }

            THEN("the model has the constraint we just added")
            {
                const auto &constraint = milpModel.C(constraintName);

                REQUIRE(constraint.upperBound() == 10.);
                REQUIRE(constraint.lowerBound() == std::numeric_limits<double>::lowest());
                REQUIRE(constraint.name() == constraintName);

                const auto &le = constraint.linearExpression();

                const double constantPart = le.constant();
                const auto & varCoeffs    = le.coeffs();

                REQUIRE(constantPart == 0.);
                REQUIRE(varCoeffs.size() == 2);

                const auto &x = milpModel.V("x");
                const auto &y = milpModel.V("y");

                REQUIRE(varCoeffs.at(&x) == 1.);
                REQUIRE(varCoeffs.at(&y) == 3.);
            }

            AND_WHEN("adding another constraint to the model")
            {
                constexpr auto constraintName2 = "cstr 2";

                milpModel.addConstraint(constraintName2, [](const MilpModel &model) {
                    const auto &x = model.V("x");
                    const auto &y = model.V("y");

                    return 10. >= x + 3 * y + 1.2;
                });

                THEN("the model has 2 constraint")
                {
                    REQUIRE(milpModel.numConstraints() == 2);
                }

                THEN("the model has the constraint we just added")
                {
                    const auto &constraint = milpModel.C(constraintName2);

                    REQUIRE(constraint.upperBound() == 8.8);
                    REQUIRE(constraint.lowerBound() == std::numeric_limits<double>::lowest());
                    REQUIRE(constraint.name() == constraintName2);

                    const auto &le = constraint.linearExpression();

                    const double constantPart = le.constant();
                    const auto & varCoeffs    = le.coeffs();

                    REQUIRE(constantPart == 0.);
                    REQUIRE(varCoeffs.size() == 2);

                    const auto &x = milpModel.V("x");
                    const auto &y = milpModel.V("y");

                    REQUIRE(varCoeffs.at(&x) == 1.);
                    REQUIRE(varCoeffs.at(&y) == 3.);
                }
            }
        }
    }
}

SCENARIO("constraints with a lower bound can be added to a MilpModel", "[milpModel]")
{
    GIVEN("a MilpModel instance with two variables x and y")
    {
        MilpModel milpModel;

        milpModel.addContinuousUnboundedVariable("x");
        milpModel.addContinuousUnboundedVariable("y");

        WHEN("adding a new constraint to the model with lower bound as RHS")
        {
            constexpr auto constraintName = "cstr 1";

            milpModel.addConstraint(constraintName, [](const MilpModel &model) {
                const auto &x = model.V("x");
                const auto &y = model.V("y");

                return x + 3 * y >= 10.;
            });

            THEN("the model has 1 constraint")
            {
                REQUIRE(milpModel.numConstraints() == 1);
            }

            THEN("the model has the constraint we just added")
            {
                const auto &constraint = milpModel.C(constraintName);

                REQUIRE(constraint.upperBound() == std::numeric_limits<double>::max());
                REQUIRE(constraint.lowerBound() == 10.);
                REQUIRE(constraint.name() == constraintName);

                const auto &le = constraint.linearExpression();

                const double constantPart = le.constant();
                const auto & varCoeffs    = le.coeffs();

                REQUIRE(constantPart == 0.);
                REQUIRE(varCoeffs.size() == 2);

                const auto &x = milpModel.V("x");
                const auto &y = milpModel.V("y");

                REQUIRE(varCoeffs.at(&x) == 1.);
                REQUIRE(varCoeffs.at(&y) == 3.);
            }

            AND_WHEN("adding another constraint to the model")
            {
                constexpr auto constraintName2 = "cstr 2";

                milpModel.addConstraint(constraintName2, [](const MilpModel &model) {
                    const auto &x = model.V("x");
                    const auto &y = model.V("y");

                    return x + 3 * y + 1.2 >= 10.;
                });

                THEN("the model has 2 constraint")
                {
                    REQUIRE(milpModel.numConstraints() == 2);
                }

                THEN("the model has the constraint we just added")
                {
                    const auto &constraint = milpModel.C(constraintName2);

                    REQUIRE(constraint.upperBound() == std::numeric_limits<double>::max());
                    REQUIRE(constraint.lowerBound() == 8.8);
                    REQUIRE(constraint.name() == constraintName2);

                    const auto &le = constraint.linearExpression();

                    const double constantPart = le.constant();
                    const auto & varCoeffs    = le.coeffs();

                    REQUIRE(constantPart == 0.);
                    REQUIRE(varCoeffs.size() == 2);

                    const auto &x = milpModel.V("x");
                    const auto &y = milpModel.V("y");

                    REQUIRE(varCoeffs.at(&x) == 1.);
                    REQUIRE(varCoeffs.at(&y) == 3.);
                }
            }
        }

        WHEN("adding a new constraint to the model with lower bound as LHS")
        {
            constexpr auto constraintName = "cstr 1";

            milpModel.addConstraint(constraintName, [](const MilpModel &model) {
                const auto &x = model.V("x");
                const auto &y = model.V("y");

                return 10. <= x + 3 * y;
            });

            THEN("the model has 1 constraint")
            {
                REQUIRE(milpModel.numConstraints() == 1);
            }

            THEN("the model has the constraint we just added")
            {
                const auto &constraint = milpModel.C(constraintName);

                REQUIRE(constraint.upperBound() == std::numeric_limits<double>::max());
                REQUIRE(constraint.lowerBound() == 10.);
                REQUIRE(constraint.name() == constraintName);

                const auto &le = constraint.linearExpression();

                const double constantPart = le.constant();
                const auto & varCoeffs    = le.coeffs();

                REQUIRE(constantPart == 0.);
                REQUIRE(varCoeffs.size() == 2);

                const auto &x = milpModel.V("x");
                const auto &y = milpModel.V("y");

                REQUIRE(varCoeffs.at(&x) == 1.);
                REQUIRE(varCoeffs.at(&y) == 3.);
            }

            AND_WHEN("adding another constraint to the model")
            {
                constexpr auto constraintName2 = "cstr 2";

                milpModel.addConstraint(constraintName2, [](const MilpModel &model) {
                    const auto &x = model.V("x");
                    const auto &y = model.V("y");

                    return 10. <= x + 3 * y + 1.2;
                });

                THEN("the model has 2 constraint")
                {
                    REQUIRE(milpModel.numConstraints() == 2);
                }

                THEN("the model has the constraint we just added")
                {
                    const auto &constraint = milpModel.C(constraintName2);

                    REQUIRE(constraint.upperBound() == std::numeric_limits<double>::max());
                    REQUIRE(constraint.lowerBound() == 8.8);
                    REQUIRE(constraint.name() == constraintName2);

                    const auto &le = constraint.linearExpression();

                    const double constantPart = le.constant();
                    const auto & varCoeffs    = le.coeffs();

                    REQUIRE(constantPart == 0.);
                    REQUIRE(varCoeffs.size() == 2);

                    const auto &x = milpModel.V("x");
                    const auto &y = milpModel.V("y");

                    REQUIRE(varCoeffs.at(&x) == 1.);
                    REQUIRE(varCoeffs.at(&y) == 3.);
                }
            }
        }
    }
}

SCENARIO("equality constraints can be added to a MilpModel", "[milpModel]")
{
    GIVEN("a MilpModel instance with two variables x and y")
    {
        MilpModel milpModel;

        milpModel.addContinuousUnboundedVariable("x");
        milpModel.addContinuousUnboundedVariable("y");

        THEN("the model knows 2 variable names")
        {
            const auto allVarNamesInModel = milpModel.varNames();

            REQUIRE(allVarNamesInModel.size() == 2);
            REQUIRE(allVarNamesInModel == std::vector<std::string>({ "x", "y" }));
        }

        WHEN("adding a new equality constraint to the model with constant as RHS")
        {
            constexpr auto constraintName = "cstr 1";

            milpModel.addConstraint(constraintName, [](const MilpModel &model) {
                const auto &x = model.V("x");
                const auto &y = model.V("y");

                return x + 3 * y == 10.;
            });

            THEN("the model has 1 constraint")
            {
                REQUIRE(milpModel.numConstraints() == 1);
            }

            THEN("the model has the constraint we just added")
            {
                const auto &constraint = milpModel.C(constraintName);

                REQUIRE(constraint.upperBound() == 10.);
                REQUIRE(constraint.lowerBound() == 10.);
                REQUIRE(constraint.name() == constraintName);

                const auto &le = constraint.linearExpression();

                const double constantPart = le.constant();
                const auto & varCoeffs    = le.coeffs();

                REQUIRE(constantPart == 0.);
                REQUIRE(varCoeffs.size() == 2);

                const auto &x = milpModel.V("x");
                const auto &y = milpModel.V("y");

                REQUIRE(varCoeffs.at(&x) == 1.);
                REQUIRE(varCoeffs.at(&y) == 3.);
            }

            AND_WHEN("adding another equality constraint to the model")
            {
                constexpr auto constraintName2 = "cstr 2";

                milpModel.addConstraint(constraintName2, [](const MilpModel &model) {
                    const auto &x = model.V("x");
                    const auto &y = model.V("y");

                    return x + 3 * y + 1.2 == 10.;
                });

                THEN("the model has 2 constraint")
                {
                    REQUIRE(milpModel.numConstraints() == 2);
                }

                THEN("the model has the constraint we just added")
                {
                    const auto &constraint = milpModel.C(constraintName2);

                    REQUIRE(constraint.upperBound() == 8.8);
                    REQUIRE(constraint.lowerBound() == 8.8);
                    REQUIRE(constraint.name() == constraintName2);

                    const auto &le = constraint.linearExpression();

                    const double constantPart = le.constant();
                    const auto & varCoeffs    = le.coeffs();

                    REQUIRE(constantPart == 0.);
                    REQUIRE(varCoeffs.size() == 2);

                    const auto &x = milpModel.V("x");
                    const auto &y = milpModel.V("y");

                    REQUIRE(varCoeffs.at(&x) == 1.);
                    REQUIRE(varCoeffs.at(&y) == 3.);
                }
            }
        }

        WHEN("adding a new equality constraint to the model with constant as LHS")
        {
            constexpr auto constraintName = "cstr 1";

            milpModel.addConstraint(constraintName, [](const MilpModel &model) {
                const auto &x = model.V("x");
                const auto &y = model.V("y");

                return 10. == x + 3 * y;
            });

            THEN("the model has 1 constraint")
            {
                REQUIRE(milpModel.numConstraints() == 1);
            }

            THEN("the model has the constraint we just added")
            {
                const auto &constraint = milpModel.C(constraintName);

                REQUIRE(constraint.upperBound() == 10.);
                REQUIRE(constraint.lowerBound() == 10.);
                REQUIRE(constraint.name() == constraintName);

                const auto &le = constraint.linearExpression();

                const double constantPart = le.constant();
                const auto & varCoeffs    = le.coeffs();

                REQUIRE(constantPart == 0.);
                REQUIRE(varCoeffs.size() == 2);

                const auto &x = milpModel.V("x");
                const auto &y = milpModel.V("y");

                REQUIRE(varCoeffs.at(&x) == 1.);
                REQUIRE(varCoeffs.at(&y) == 3.);
            }

            AND_WHEN("adding another equality constraint to the model")
            {
                constexpr auto constraintName2 = "cstr 2";

                milpModel.addConstraint(constraintName2, [](const MilpModel &model) {
                    const auto &x = model.V("x");
                    const auto &y = model.V("y");

                    return 10. == x + 3 * y + 1.2;
                });

                THEN("the model has 2 constraint")
                {
                    REQUIRE(milpModel.numConstraints() == 2);
                }

                THEN("the model has the constraint we just added")
                {
                    const auto &constraint = milpModel.C(constraintName2);

                    REQUIRE(constraint.upperBound() == 8.8);
                    REQUIRE(constraint.lowerBound() == 8.8);
                    REQUIRE(constraint.name() == constraintName2);

                    const auto &le = constraint.linearExpression();

                    const double constantPart = le.constant();
                    const auto & varCoeffs    = le.coeffs();

                    REQUIRE(constantPart == 0.);
                    REQUIRE(varCoeffs.size() == 2);

                    const auto &x = milpModel.V("x");
                    const auto &y = milpModel.V("y");

                    REQUIRE(varCoeffs.at(&x) == 1.);
                    REQUIRE(varCoeffs.at(&y) == 3.);
                }
            }
        }
    }
}

SCENARIO("indexed constraints can be added to a MilpModel", "[milpModel]")
{
    GIVEN("a MilpModel instance with two variables x and y")
    {
        MilpModel milpModel;

        milpModel.addContinuousUnboundedVariable("x");
        milpModel.addContinuousUnboundedVariable("y");

        AND_GIVEN("an index set I consisting of integer indices")
        {
            RangeSet I(3, 10);

            WHEN("adding constraints indexed by I")
            {
                constexpr auto cstrName = "myIndexedCstr";

                milpModel.addConstraint(
                    cstrName,
                    [](const MilpModel &model, int i) {
                        const auto &x = model.V("x");
                        const auto &y = model.V("y");

                        return 10. * i <= x + 3 * y + 1.2;
                    },
                    I);

                THEN("the MilpModel has 7 constraints")
                {
                    REQUIRE(milpModel.numConstraints() == 7);
                }

                THEN("the model has the constraints we just added")
                {
                    for (auto i : I)
                    {
                        CAPTURE(i);

                        const auto &constraint = milpModel.C(cstrName, { i });

                        REQUIRE(constraint.upperBound() == std::numeric_limits<double>::max());
                        REQUIRE(constraint.lowerBound() == 10. * i - 1.2);
                        REQUIRE(constraint.name() == fmt::format("{}_{}", cstrName, i));

                        const auto &le = constraint.linearExpression();

                        const double constantPart = le.constant();
                        const auto & varCoeffs    = le.coeffs();

                        REQUIRE(constantPart == 0.);
                        REQUIRE(varCoeffs.size() == 2);

                        const auto &x = milpModel.V("x");
                        const auto &y = milpModel.V("y");

                        REQUIRE(varCoeffs.at(&x) == 1.);
                        REQUIRE(varCoeffs.at(&y) == 3.);
                    }
                }
            }

            WHEN("adding constraints indexed by IxI")
            {
                constexpr auto cstrName = "myIndexedCstr";

                milpModel.addConstraint(
                    cstrName,
                    [](const MilpModel &model, int i, int j) {
                        const auto &x = model.V("x");
                        const auto &y = model.V("y");

                        return 10. * i <= j * x + 3 * y + 1.2;
                    },
                    I, I);

                THEN("the MilpModel has 49 constraints")
                {
                    REQUIRE(milpModel.numConstraints() == 49);
                }

                THEN("the model knows 1 constraint by name")
                {
                    const auto allCstrNamesInModel = milpModel.constraintNames();

                    REQUIRE(allCstrNamesInModel.size() == 1);
                    REQUIRE(allCstrNamesInModel == std::vector<std::string>({ cstrName }));
                }

                THEN("the model has the constraints we just added")
                {
                    fmapCartesianProduct(
                        [&milpModel, &cstrName](int i, int j) {
                            CAPTURE(i, j);

                            const auto &constraint = milpModel.C(cstrName, { i, j });

                            REQUIRE(constraint.upperBound() == std::numeric_limits<double>::max());
                            REQUIRE(constraint.lowerBound() == 10. * i - 1.2);
                            REQUIRE(constraint.name() == fmt::format("{}_{}_{}", cstrName, i, j));

                            const auto &le = constraint.linearExpression();

                            const double constantPart = le.constant();
                            const auto & varCoeffs    = le.coeffs();

                            REQUIRE(constantPart == 0.);
                            REQUIRE(varCoeffs.size() == 2);

                            const auto &x = milpModel.V("x");
                            const auto &y = milpModel.V("y");

                            REQUIRE(varCoeffs.at(&x) == j);
                            REQUIRE(varCoeffs.at(&y) == 3.);
                        },
                        I, I);
                }
            }

            WHEN("adding constraints indexed by I and skipping constraints for 1 index")
            {
                constexpr auto cstrName = "myIndexedCstr";

                milpModel.addConstraint(
                    cstrName,
                    [](const MilpModel &model, int i) {
                        if (i == 5)
                            return Constraint(Marker::Skip);

                        const auto &x = model.V("x");
                        const auto &y = model.V("y");

                        return 10. * i <= x + 3 * y + 1.2;
                    },
                    I);

                THEN("the MilpModel has 6 constraints")
                {
                    REQUIRE(milpModel.numConstraints() == 6);
                }

                THEN("the model has the constraints we just added")
                {
                    for (auto i : I)
                    {
                        CAPTURE(i);

                        const Constraint *cstr = nullptr;

                        if (i == 5)
                        {
                            REQUIRE_THROWS(milpModel.C(cstrName, { i }));
                            continue;
                        }

                        cstr = &milpModel.C(cstrName, { i });

                        const auto &constraint = *cstr;

                        REQUIRE(constraint.upperBound() == std::numeric_limits<double>::max());
                        REQUIRE(constraint.lowerBound() == 10. * i - 1.2);
                        REQUIRE(constraint.name() == fmt::format("{}_{}", cstrName, i));

                        const auto &le = constraint.linearExpression();

                        const double constantPart = le.constant();
                        const auto & varCoeffs    = le.coeffs();

                        REQUIRE(constantPart == 0.);
                        REQUIRE(varCoeffs.size() == 2);

                        const auto &x = milpModel.V("x");
                        const auto &y = milpModel.V("y");

                        REQUIRE(varCoeffs.at(&x) == 1.);
                        REQUIRE(varCoeffs.at(&y) == 3.);
                    }
                }
            }
        }

        AND_GIVEN("an index set I consisting of string indices")
        {
            StringSet I({ "a", "bb", "ccc", "dddd", "eeeee", "ffffff", "ggggggg" });

            WHEN("adding constraints indexed by I")
            {
                constexpr auto cstrName = "myIndexedCstr";

                milpModel.addConstraint(
                    cstrName,
                    [](const MilpModel &model, const std::string &i) {
                        const auto &x = model.V("x");
                        const auto &y = model.V("y");

                        return 10. <= x + 3 * y + 1.2;
                    },
                    I);

                THEN("the MilpModel has 7 constraints")
                {
                    REQUIRE(milpModel.numConstraints() == 7);
                }

                THEN("the model has the constraints we just added")
                {
                    for (auto i : I)
                    {
                        CAPTURE(i);

                        const auto &constraint = milpModel.C(cstrName, { i });

                        REQUIRE(constraint.upperBound() == std::numeric_limits<double>::max());
                        REQUIRE(constraint.name() == fmt::format("{}_{}", cstrName, i));

                        const auto &le = constraint.linearExpression();

                        const double constantPart = le.constant();
                        const auto & varCoeffs    = le.coeffs();

                        REQUIRE(constantPart == 0.);
                        REQUIRE(varCoeffs.size() == 2);

                        const auto &x = milpModel.V("x");
                        const auto &y = milpModel.V("y");

                        REQUIRE(varCoeffs.at(&x) == 1.);
                        REQUIRE(varCoeffs.at(&y) == 3.);
                    }
                }
            }

            WHEN("adding constraints indexed by IxI")
            {
                constexpr auto cstrName = "myIndexedCstr";

                milpModel.addConstraint(
                    cstrName,
                    [](const MilpModel &model, const std::string &i, const std::string &j) {
                        const auto &x = model.V("x");
                        const auto &y = model.V("y");

                        return 10. <= x + 3 * y + 1.2;
                    },
                    I, I);

                THEN("the MilpModel has 49 constraints")
                {
                    REQUIRE(milpModel.numConstraints() == 49);
                }

                THEN("the model knows 1 constraint by name")
                {
                    const auto allCstrNamesInModel = milpModel.constraintNames();

                    REQUIRE(allCstrNamesInModel.size() == 1);
                    REQUIRE(allCstrNamesInModel == std::vector<std::string>({ cstrName }));
                }

                THEN("the model has the constraints we just added")
                {
                    fmapCartesianProduct(
                        [&milpModel, &cstrName](const std::string &i, const std::string &j) {
                            CAPTURE(i, j);

                            const auto &constraint = milpModel.C(cstrName, { i, j });

                            REQUIRE(constraint.upperBound() == std::numeric_limits<double>::max());
                            REQUIRE(constraint.lowerBound() == 8.8);
                            REQUIRE(constraint.name() == fmt::format("{}_{}_{}", cstrName, i, j));

                            const auto &le = constraint.linearExpression();

                            const double constantPart = le.constant();
                            const auto & varCoeffs    = le.coeffs();

                            REQUIRE(constantPart == 0.);
                            REQUIRE(varCoeffs.size() == 2);

                            const auto &x = milpModel.V("x");
                            const auto &y = milpModel.V("y");

                            REQUIRE(varCoeffs.at(&y) == 3.);
                        },
                        I, I);
                }
            }

            WHEN("adding constraints indexed by I and skipping constraints for 1 index")
            {
                constexpr auto cstrName = "myIndexedCstr";

                milpModel.addConstraint(
                    cstrName,
                    [](const MilpModel &model, const std::string &i) {
                        if (i == "eeeee")
                            return Constraint(Marker::Skip);

                        const auto &x = model.V("x");
                        const auto &y = model.V("y");

                        return 10. <= x + 3 * y + 1.2;
                    },
                    I);

                THEN("the MilpModel has 6 constraints")
                {
                    REQUIRE(milpModel.numConstraints() == 6);
                }

                THEN("the model has the constraints we just added")
                {
                    for (auto i : I)
                    {
                        CAPTURE(i);

                        const Constraint *cstr = nullptr;

                        if (i == "eeeee")
                        {
                            REQUIRE_THROWS(milpModel.C(cstrName, { i }));
                            continue;
                        }

                        cstr = &milpModel.C(cstrName, { i });

                        const auto &constraint = *cstr;

                        REQUIRE(constraint.upperBound() == std::numeric_limits<double>::max());
                        REQUIRE(constraint.name() == fmt::format("{}_{}", cstrName, i));

                        const auto &le = constraint.linearExpression();

                        const double constantPart = le.constant();
                        const auto & varCoeffs    = le.coeffs();

                        REQUIRE(constantPart == 0.);
                        REQUIRE(varCoeffs.size() == 2);

                        const auto &x = milpModel.V("x");
                        const auto &y = milpModel.V("y");

                        REQUIRE(varCoeffs.at(&x) == 1.);
                        REQUIRE(varCoeffs.at(&y) == 3.);
                    }
                }
            }
        }
    }
}

SCENARIO("parameters can be added to a MilpModel and queried", "[milpModel]")
{
    GIVEN("a new MilpModel")
    {
        MilpModel milpModel;

        THEN("the model does not have a parameter of name 'p1'")
        {
            REQUIRE_THROWS(milpModel.P("p1"));
        }

        WHEN("adding a parameter 'p1' without index to the model, with value 1.1")
        {
            {
                Parameter p1("p1");
                p1.setValue({}, 1.1);

                milpModel.setParameter(std::move(p1));
            }

            THEN("the model has a parameter of name 'p1'")
            {
                REQUIRE_NOTHROW(milpModel.P("p1"));

                AND_THEN("the model's parameter of name 'p1' has value 1.1 without an index")
                {
                    const auto &p1 = milpModel.P("p1");

                    REQUIRE(p1.value({}) == 1.1);
                }
            }
        }

        WHEN("adding a parameter 'p1' indexed by integers from 0 to 4 to the model, with value 1.1, 2.2, ..., 5.5")
        {
            RangeSet I(5);

            {
                Parameter p1("p1");

                for (auto i : I)
                {
                    p1.setValue({ i }, 1.1 * (i + 1));
                }

                milpModel.setParameter(std::move(p1));
            }

            THEN("the model has a parameter of name 'p1'")
            {
                REQUIRE_NOTHROW(milpModel.P("p1"));

                AND_THEN("the model's parameter of name 'p1' has value 1.1 without an index")
                {
                    const auto &p1 = milpModel.P("p1");

                    for (auto i : I)
                    {
                        REQUIRE(p1.value({ i }) == 1.1 * (i + 1));
                    }
                }
            }
        }
    }
}
