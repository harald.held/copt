#include <catch2/catch.hpp>

#include <Parameter.h>
#include <RangeSet.h>
#include <StringSet.h>

using namespace copt;

SCENARIO("a new parameter can be assigned indexed values", "[parameter]")
{
    GIVEN("a new, empty parameter named 'p1'")
    {
        Parameter param("p1");

        WHEN("querying that parameters name")
        {
            const auto paramName = param.name();

            THEN("that name equals 'p1'")
            {
                REQUIRE(paramName == "p1");
            }
        }

        THEN("querying a parameter value for an index that does not have a value throws an exception")
        {
            REQUIRE_THROWS(param.value({}));

            // this is deliberately done twice here to ensure that the first call doesn't accidentally create a value in
            // the underlying map!
            REQUIRE_THROWS(param.value({}));

            REQUIRE_THROWS(param.value({ 1 }));
            REQUIRE_THROWS(param.value({ "a" }));
        }

        WHEN("setting a parameter value w/o, i.e. empty index")
        {
            param.setValue({}, 1.1);

            THEN("the parameter can be queried for that index giving back the value set before")
            {
                const auto value = param.value({});

                REQUIRE(value == 1.1);
            }
        }

        AND_GIVEN("a RangeSet I and a StringSet J")
        {
            RangeSet  I(3);
            StringSet J({ "a", "bb" });

            WHEN("setting parameter values for each index in IxJ")
            {
                int counter = 0;

                for (const auto &i : I)
                {
                    for (const auto &j : J)
                    {
                        param.setValue({ i, j }, counter++);
                    }
                }

                THEN("the parameter values are what has been just set")
                {
                    int counterQuery = 0;

                    for (const auto &i : I)
                    {
                        for (const auto &j : J)
                        {
                            REQUIRE(param.value({ i, j }) == counterQuery++);
                        }
                    }
                }
            }
        }
    }
}
