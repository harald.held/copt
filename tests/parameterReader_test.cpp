#include <catch2/catch.hpp>

#include <ParameterReader.h>

using namespace copt;

SCENARIO("model parameters can be read from a YAML file", "[parameterReader]")
{
    GIVEN("a YAML file and an associated ParameterReader instance")
    {
        constexpr auto  yamlFilePath = "./resources/dietData.yml";
        ParameterReader pr(yamlFilePath);

        THEN("the reader read 7 parameters from the file")
        {
            REQUIRE(pr.params().size() == 7);
        }
    }
}
