#include <catch2/catch.hpp>

#include <CartesianProduct.h>
#include <RangeSet.h>
#include <StringSet.h>

using namespace copt;

TEST_CASE("Cartesian product", "[cartesianProduct]")
{
    RangeSet I(4, 11);

    int counter = 0;

    fmapCartesianProduct([&counter](int /*i*/, int /*j*/, int /*k*/) { ++counter; }, I, I, I);

    REQUIRE(counter == 343);
}

TEST_CASE("Cartesian product can be filtered", "[cartesianProduct]")
{
    RangeSet I(4, 11);

    int counter = 0;

    auto filter = [](int i, int j, int k) { return i + j + k <= 15; };

    fmapCartesianProductWithFilter([&counter](int /*i*/, int /*j*/, int /*k*/) { ++counter; }, filter, I, I, I);

    REQUIRE(counter == 20);
}

TEST_CASE("Cartesian product also works with StringSets", "[cartesianProduct]")
{
    StringSet I({ "a", "bb", "ccc" });

    int counter = 0;

    fmapCartesianProduct(
        [&counter](const std::string & /*i*/, const std::string & /*j*/, const std::string & /*k*/) { ++counter; }, I,
        I, I);

    REQUIRE(counter == 27);
}

TEST_CASE("Cartesian product with StringSets can be filtered", "[cartesianProduct]")
{
    StringSet I({ "a", "bb", "ccc" });

    int counter = 0;

    auto filter = [](const std::string &i, const std::string &j, const std::string &k) {
        return i == "a" && j == "bb";
    };

    fmapCartesianProductWithFilter(
        [&counter](const std::string & /*i*/, const std::string & /*j*/, const std::string & /*k*/) { ++counter; },
        filter, I, I, I);

    REQUIRE(counter == 3);
}

TEST_CASE("Cartesian product with a mixture of RangeSets and StringSets can be filtered", "[cartesianProduct]")
{
    StringSet I({ "a", "bb", "ccc" });
    RangeSet  J(4);

    int counter = 0;

    auto filter = [](const std::string &i, const std::string &j, const std::string &k, int l, int m) {
        return i == "a" && j == "bb";
    };

    fmapCartesianProductWithFilter([&counter](const std::string & /*i*/, const std::string & /*j*/,
                                              const std::string & /*k*/, int /*l*/, int /*m*/) { ++counter; },
                                   filter, I, I, I, J, J);

    REQUIRE(counter == 48);
}
