#include <catch2/catch.hpp>

#include <FilteredIndexSet.h>
#include <StringSet.h>

using namespace copt;

SCENARIO("a new StringSet can be created and iterated over", "[StringSet]")
{
    GIVEN("a new StringSet containing the strings 'a', 'bb', 'ccc'")
    {
        StringSet I({ "a", "bb", "ccc" });

        THEN("the StringSet contains 3 elements")
        {
            REQUIRE(I.count() == 3);
        }

        THEN("the StringSet is not empty")
        {
            REQUIRE(!I.empty());
        }

        THEN("the StringSet can be iterated over")
        {
            int counter = 0;

            for (const auto &i : I)
            {
                ++counter;
            }

            REQUIRE(counter == 3);
        }

        THEN("std algorithms that require forward iterators like std::copy work with StringSet")
        {
            std::vector<std::string> v;
            std::copy(std::begin(I), std::end(I), std::back_inserter(v));

            REQUIRE(v[0] == "a");
            REQUIRE(v[1] == "bb");
            REQUIRE(v[2] == "ccc");
        }

        WHEN("filtering indices starting with 'a' from that StringSet via the decorator FilteredIndexSet")
        {
            FilteredIndexSet I_startWithA(&I, [](const std::string &i) { return i.size() > 0 && i[0] == 'a'; });

            THEN("the filtered IndexSet has 1 element")
            {
                REQUIRE(I_startWithA.count() == 1);
            }

            THEN("the filtered IndexSet is not empty")
            {
                REQUIRE(!I_startWithA.empty());
            }

            THEN("the IndexSet contains 'a'")
            {
                std::vector<std::string> indices;

                for (const auto &i : I_startWithA)
                {
                    indices.push_back(i);
                }

                REQUIRE(indices == std::vector<std::string>{ "a" });
            }
        }
    }
}
