#ifndef COPT_INSTANCEINTERFACE_INCLUDE_INSTANCEINTERFACE_H
#define COPT_INSTANCEINTERFACE_INCLUDE_INSTANCEINTERFACE_H

#include <string>
#include <vector>

namespace copt
{

class MilpModel;
class Parameter;

class InstanceInterface
{
public:
    static constexpr auto createFuncName  = "createInstance";
    static constexpr auto releaseFuncName = "releaseInstance";

    using createInstanceFuncType  = InstanceInterface *(*)();
    using releaseInstanceFuncType = void (*)(InstanceInterface *);

    inline virtual ~InstanceInterface() = default;

    [[nodiscard]] virtual MilpModel *              instance() const                = 0;
    [[nodiscard]] virtual std::vector<std::string> expectedParameters() const      = 0;
    virtual void                                   setParameter(Parameter &&param) = 0;
    virtual void                                   buildModel()                    = 0;

    virtual void printSolutionToStdout() const
    {}
};

} // namespace copt

#endif // COPT_INSTANCEINTERFACE_INCLUDE_INSTANCEINTERFACE_H
