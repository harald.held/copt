#ifndef LOGGER_H
#define LOGGER_H

#include <memory>

#include <spdlog/logger.h>

namespace glpkSolverPlugin
{

std::shared_ptr<spdlog::logger> logger();

} // namespace ${PROJECT_NAME}

#endif // LOGGER_H
