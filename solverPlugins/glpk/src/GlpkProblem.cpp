#include "GlpkProblem.h"

namespace glpkSolverPlugin
{

namespace
{

constexpr auto SOLVER_NAME = "glpk";

}

GlpkProblem::GlpkProblem()
    : log_(logger())
    , prob_(glp_create_prob())
{
    glp_init_smcp(&lpOptions_);
    glp_init_iocp(&milpOptions_);

    milpOptions_.presolve = GLP_ON;

    log_->info("created a new GLPK instance");
}

GlpkProblem::~GlpkProblem()
{
    glp_delete_prob(prob_);
}

copt::SolverStatus GlpkProblem::runSolver()
{
    const auto numIntegerVars = glp_get_num_int(prob_);

    if (numIntegerVars != 0)
    {
        log_->info("problem has {} integer (incl. binary) variables; using GLPK's intopt method", numIntegerVars);

        glp_intopt(prob_, &milpOptions_);
    }
    else
    {
        log_->info("problem has only continuous variables; using GLPK's simplex method");

        glp_simplex(prob_, &lpOptions_);
    }

    return copt::SolverStatus::Optimal;
}

void GlpkProblem::addBinaryVariable(std::string_view name, size_t index)
{
    glp_add_cols(prob_, 1);
    glp_set_col_name(prob_, index + 1, name.data());
    glp_set_col_kind(prob_, index + 1, GLP_BV);
}

void GlpkProblem::addIntegerVariable(std::string_view name, size_t index, double lb, double ub)
{
    addContinuousVariable(name, index, lb, ub);
    glp_set_col_kind(prob_, index + 1, GLP_IV);
}

void GlpkProblem::addContinuousVariable(std::string_view name, size_t index, double lb, double ub)
{
    glp_add_cols(prob_, 1);
    glp_set_col_name(prob_, index + 1, name.data());
    glp_set_col_kind(prob_, index + 1, GLP_CV);

    const bool hasLowerBound = lb != std::numeric_limits<double>::lowest();
    const bool hasUpperBound = ub != std::numeric_limits<double>::max();
    const bool isFixedVar    = lb == ub;

    if (hasLowerBound)
    {
        if (hasUpperBound)
        {
            // variable has lower and upper bounds
            glp_set_col_bnds(prob_, index + 1, isFixedVar ? GLP_FX : GLP_DB, lb, ub);
        }
        else
        {
            // variable has only lower bound
            glp_set_col_bnds(prob_, index + 1, GLP_LO, lb, 0.);
        }
    }
    else
    {
        if (hasUpperBound)
        {
            // variable has upper bound
            glp_set_col_bnds(prob_, index + 1, GLP_UP, 0., ub);
        }
        else
        {
            // variable has no bounds
            glp_set_col_bnds(prob_, index + 1, GLP_FR, 0., 0.);
        }
    }
}

void GlpkProblem::addConstraint(std::string_view name, std::vector<VarCoeff> &&coeffs, size_t index, double lb,
                                double ub)
{
    glp_add_rows(prob_, 1);
    glp_set_row_name(prob_, index + 1, name.data());

    const bool hasLowerBound = lb != std::numeric_limits<double>::lowest();
    const bool hasUpperBound = ub != std::numeric_limits<double>::max();
    const bool isEquality    = lb == ub;

    if (hasLowerBound)
    {
        if (hasUpperBound)
        {
            // variable has lower and upper bounds
            glp_set_row_bnds(prob_, index + 1, isEquality ? GLP_FX : GLP_DB, lb, ub);
        }
        else
        {
            // variable has only lower bound
            glp_set_row_bnds(prob_, index + 1, GLP_LO, lb, 0.);
        }
    }
    else
    {
        if (hasUpperBound)
        {
            // variable has upper bound
            glp_set_row_bnds(prob_, index + 1, GLP_UP, 0., ub);
        }
        else
        {
            // variable has no bounds
            glp_set_row_bnds(prob_, index + 1, GLP_FR, 0., 0.);
        }
    }

    std::vector<int>    indices(coeffs.size() + 1);
    std::vector<double> values(coeffs.size() + 1);

    for (size_t i = 1, len = indices.size(); i < len; ++i)
    {
        const auto &vc = coeffs[i - 1];

        indices[i] = vc.index + 1;
        values[i]  = vc.coefficient;
    }

    glp_set_mat_row(prob_, index + 1, coeffs.size(), indices.data(), values.data());
}

void GlpkProblem::setObjective(std::string_view name, std::vector<VarCoeff> &&coeffs, copt::ObjectiveSense objSense)
{
    glp_set_obj_name(prob_, name.data());

    switch (objSense)
    {
    case copt::ObjectiveSense::Minimize:
        glp_set_obj_dir(prob_, GLP_MIN);
        break;
    case copt::ObjectiveSense::Maximize:
        glp_set_obj_dir(prob_, GLP_MAX);
        break;
    }

    for (const auto &vc : coeffs)
    {
        glp_set_obj_coef(prob_, vc.index + 1, vc.coefficient);
    }
}

double GlpkProblem::variableValue(size_t index) const
{
    const auto numIntegerVars = glp_get_num_int(prob_);

    if (numIntegerVars != 0)
    {
        return glp_mip_col_val(prob_, index + 1);
    }

    return glp_get_col_prim(prob_, index + 1);
}

void GlpkProblem::applySolverOptions()
{
    switch (options_.outputLevel)
    {
    case copt::Options::OutputLevel::None:
        lpOptions_.msg_lev   = GLP_MSG_OFF;
        milpOptions_.msg_lev = GLP_MSG_OFF;
        break;
    case copt::Options::OutputLevel::Error:
        lpOptions_.msg_lev   = GLP_MSG_ERR;
        milpOptions_.msg_lev = GLP_MSG_ERR;
        break;
    case copt::Options::OutputLevel::Normal:
        lpOptions_.msg_lev   = GLP_MSG_ON;
        milpOptions_.msg_lev = GLP_MSG_ON;
        break;
    case copt::Options::OutputLevel::Verbose:
        lpOptions_.msg_lev   = GLP_MSG_ALL;
        milpOptions_.msg_lev = GLP_MSG_ALL;
        break;
    }
}

} // namespace glpkSolverPlugin

copt::SolverInterface *createInstance()
{
    return new glpkSolverPlugin::GlpkProblem;
}

void releaseInstance(copt::SolverInterface *solver)
{
    delete solver;
}

const char *solverName()
{
    return glpkSolverPlugin::SOLVER_NAME;
}
