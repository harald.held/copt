#ifndef COPT_GLPKPROBLEM_H
#define COPT_GLPKPROBLEM_H

#include "logger.h"

#include <SolverInterface.h>
#include <glpk.h>

struct glp_prob;

namespace glpkSolverPlugin
{

class GlpkProblem : public copt::SolverInterface
{
public:
    GlpkProblem();
    ~GlpkProblem() override;

    void addBinaryVariable(std::string_view name, size_t index) override;
    void addIntegerVariable(std::string_view name, size_t index, double lb, double ub) override;
    void addContinuousVariable(std::string_view name, size_t index, double lb, double ub) override;

    void addConstraint(std::string_view name, std::vector<VarCoeff> &&coeffs, size_t index, double lb,
                       double ub) override;
    void setObjective(std::string_view name, std::vector<VarCoeff> &&coeffs, copt::ObjectiveSense objSense) override;

    [[nodiscard]] double variableValue(size_t index) const override;

private:
    std::shared_ptr<spdlog::logger> log_;
    glp_prob *                      prob_;

    glp_smcp lpOptions_;
    glp_iocp milpOptions_;

    void               applySolverOptions() override;
    copt::SolverStatus runSolver() override;
};

} // namespace glpkSolverPlugin

// plugin factory ######################################################################################################

#ifdef __cplusplus
extern "C" {
#endif

copt::SolverInterface *createInstance();
void                   releaseInstance(copt::SolverInterface *solver);
const char *           solverName();

#ifdef __cplusplus
};
#endif

#endif // COPT_GLPKPROBLEM_H
