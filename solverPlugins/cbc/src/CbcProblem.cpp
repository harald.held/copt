#include "CbcProblem.h"

#include <CbcSolver.hpp>
#include <CoinPackedMatrix.hpp>
#include <OsiCbcSolverInterface.hpp>

namespace cbcSolverPlugin
{

namespace
{

constexpr auto SOLVER_NAME = "cbc";

}

CbcProblem::CbcProblem()
    : log_(logger())
    , prob_(std::make_unique<OsiCbcSolverInterface>())
    , matrix_(std::make_unique<CoinPackedMatrix>(false, 0, 0))
{
    log_->info("created a new Cbc instance");
}

CbcProblem::~CbcProblem() = default;

void CbcProblem::giveSizeHint(size_t numConstraints, size_t numVariables)
{
    matrix_->reserve(numConstraints, 3 * numVariables * numConstraints);

    obj_.reserve(numVariables);
    colLb_.reserve(numVariables);
    colUb_.reserve(numVariables);

    rowLb_.reserve(numConstraints);
    rowUb_.reserve(numConstraints);
}

copt::SolverStatus CbcProblem::runSolver()
{
    prob_->loadProblem(*matrix_, colLb_.data(), colUb_.data(), obj_.data(), rowLb_.data(), rowUb_.data());
    prob_->setInteger(integerIndices_.data(), integerIndices_.size());

    model_ = std::make_unique<CbcModel>(*prob_);

    model_->initialSolve();
    model_->branchAndBound();

    solution_ = model_->getColSolution();

    return copt::SolverStatus::Optimal;
}

void CbcProblem::addBinaryVariable(std::string_view name, size_t index)
{
    CoinPackedVector empty;

    matrix_->appendCol(empty);
    colLb_.push_back(0.);
    colUb_.push_back(1.);

    integerIndices_.push_back(index);
}

void CbcProblem::addIntegerVariable(std::string_view name, size_t index, double lb, double ub)
{
    CoinPackedVector empty;

    matrix_->appendCol(empty);
    colLb_.push_back(lb);
    colUb_.push_back(ub);

    integerIndices_.push_back(index);
}

void CbcProblem::addContinuousVariable(std::string_view name, size_t index, double lb, double ub)
{
    CoinPackedVector empty;

    matrix_->appendCol(empty);
    colLb_.push_back(lb);
    colUb_.push_back(ub);
}

void CbcProblem::addConstraint(std::string_view name, std::vector<VarCoeff> &&coeffs, size_t index, double lb,
                               double ub)
{
    (void)index;

    CoinPackedVector row;
    row.reserve(coeffs.size());

    for (const auto &coeff : coeffs)
    {
        row.insert(coeff.index, coeff.coefficient);
    }

    matrix_->appendRow(row);
    rowLb_.push_back(lb);
    rowUb_.push_back(ub);
}

void CbcProblem::setObjective(std::string_view name, std::vector<VarCoeff> &&coeffs, copt::ObjectiveSense objSense)
{
    for (const auto &coeff : coeffs)
    {
        obj_.push_back(coeff.coefficient);
    }

    prob_->setObjName(name.data());
    prob_->setObjSense(objSense == copt::ObjectiveSense::Minimize ? 1. : -1.);
}

double CbcProblem::variableValue(size_t index) const
{
    if (!solution_)
    {
        return 0.;
    }

    return solution_[index];
}

void CbcProblem::applySolverOptions()
{
    switch (options_.outputLevel)
    {
    case copt::Options::OutputLevel::None:
        prob_->setHintParam(OsiDoReducePrint, true, OsiHintDo);
        break;
    case copt::Options::OutputLevel::Error:
        break;
    case copt::Options::OutputLevel::Normal:
        break;
    case copt::Options::OutputLevel::Verbose:
        break;
    }
}

} // namespace cbcSolverPlugin

copt::SolverInterface *createInstance()
{
    return new cbcSolverPlugin::CbcProblem;
}

void releaseInstance(copt::SolverInterface *solver)
{
    delete solver;
}

const char *solverName()
{
    return cbcSolverPlugin::SOLVER_NAME;
}
