#ifndef LOGGER_H
#define LOGGER_H

#include <memory>

#include <spdlog/logger.h>

namespace cbcSolverPlugin
{

std::shared_ptr<spdlog::logger> logger();

} // namespace cbcSolverPlugin

#endif // LOGGER_H
