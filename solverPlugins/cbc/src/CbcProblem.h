#ifndef COPT_GLPKPROBLEM_H
#define COPT_GLPKPROBLEM_H

#include "logger.h"

#include <memory>
#include <vector>

#include <SolverInterface.h>

class OsiSolverInterface;
class CbcModel;
class CoinPackedMatrix;

namespace cbcSolverPlugin
{

class CbcProblem : public copt::SolverInterface
{
public:
    CbcProblem();
    ~CbcProblem() override;

    void giveSizeHint(size_t numConstraints, size_t numVariables) override;

    void addBinaryVariable(std::string_view name, size_t index) override;
    void addIntegerVariable(std::string_view name, size_t index, double lb, double ub) override;
    void addContinuousVariable(std::string_view name, size_t index, double lb, double ub) override;

    void addConstraint(std::string_view name, std::vector<VarCoeff> &&coeffs, size_t index, double lb,
                       double ub) override;
    void setObjective(std::string_view name, std::vector<VarCoeff> &&coeffs, copt::ObjectiveSense objSense) override;

    [[nodiscard]] double variableValue(size_t index) const override;

private:
    std::shared_ptr<spdlog::logger>     log_;
    std::unique_ptr<OsiSolverInterface> prob_;
    std::unique_ptr<CbcModel>           model_;
    std::unique_ptr<CoinPackedMatrix>   matrix_;
    std::vector<double>                 colUb_;
    std::vector<double>                 colLb_;
    std::vector<double>                 rowUb_;
    std::vector<double>                 rowLb_;
    std::vector<double>                 obj_;
    std::vector<int>                    integerIndices_;
    const double                       *solution_ = nullptr;

    void               applySolverOptions() override;
    copt::SolverStatus runSolver() override;
};

} // namespace cbcSolverPlugin

// plugin factory ######################################################################################################

#ifdef __cplusplus
extern "C" {
#endif

copt::SolverInterface *createInstance();
void                   releaseInstance(copt::SolverInterface *solver);
const char            *solverName();

#ifdef __cplusplus
};
#endif

#endif // COPT_GLPKPROBLEM_H
