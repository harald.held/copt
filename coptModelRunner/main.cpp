#include "InstanceManager.h"
#include "SolutionWriter.h"
#include "logger.h"

#include <MilpModel.h>
#include <MilpSolver.h>
#include <SolverOptions.h>

#include <argparse/argparse.hpp>
#include <fmt/core.h>

#include <chrono>

int main(int argc, char *argv[])
{
    argparse::ArgumentParser argParser("COPT standalone model runner");

    argParser.add_argument("-m").required().help("specify the path to the compiled model library");
    argParser.add_argument("-d")
        .default_value(std::string{})
        .help("specify the path to the YAML file with model parameters");
    argParser.add_argument("--pprint")
        .default_value(false)
        .implicit_value(true)
        .action([](const std::string &s) { return !s.empty(); })
        .help("output the actual model to stdout in human readable format");
    argParser.add_argument("--solve")
        .default_value(false)
        .implicit_value(true)
        .action([](const std::string &s) { return !s.empty(); })
        .help("solve the model instance");
    argParser.add_argument("--printSolution")
        .default_value(false)
        .implicit_value(true)
        .action([](const std::string &s) { return !s.empty(); })
        .help("print the solution according to the instance's configured presentation format");
    argParser.add_argument("--writeSolution")
        .default_value(std::string{})
        .help("file name for a YAML file containing the solution");
    argParser.add_argument("--solver").default_value(std::string{ "glpk" }).help("specify which solver to use");

    try
    {
        argParser.parse_args(argc, argv);
    }
    catch (const std::runtime_error &err)
    {
        fmt::print("could not parse commandline arguments: {}\n", err.what());
        std::cout << argParser;
        return 0;
    }

    auto logger = copt::logger();

    const auto modelPath  = argParser.get("-m");
    const auto paramPath  = argParser.get("-d");
    const auto pprint     = argParser.get<bool>("--pprint");
    const auto solve      = argParser.get<bool>("--solve");
    const auto printSol   = argParser.get<bool>("--printSolution");
    const auto resultFile = argParser.get("--writeSolution");
    const auto solverName = argParser.get("--solver");

    auto startInstanceCreationTime = std::chrono::high_resolution_clock::now();
    auto instanceManager           = copt::InstanceManager(modelPath, paramPath);

    auto *instance = instanceManager.instance();

    if (!instance)
    {
        logger->error("could not create an instance from '{}' with parameters '{}'", modelPath, paramPath);
        return 1;
    }

    auto *modelInstance = instance->instance();

    if (!modelInstance)
    {
        logger->error("did not get a valid model from '{}'", modelPath);
        return 2;
    }

    const auto endInstanceCreationTime = std::chrono::high_resolution_clock::now();

    auto instanceCreationTime =
        std::chrono::duration_cast<std::chrono::milliseconds>(endInstanceCreationTime - startInstanceCreationTime)
            .count() *
        1.E-3;

    logger->info("total instance creation time: {}", instanceCreationTime);

    if (pprint)
    {
        modelInstance->pprint();
    }

    if (solve)
    {
        auto startSolveTime = std::chrono::high_resolution_clock::now();

        copt::MilpSolver solver(*modelInstance);

        solver.options().outputLevel = copt::Options::OutputLevel::None;
        solver.solve(solverName);

        auto endSolveTime = std::chrono::high_resolution_clock::now();

        const auto solutionTime =
            std::chrono::duration_cast<std::chrono::milliseconds>(endSolveTime - startSolveTime).count() * 1.E-3;

        logger->info("solution time: {} seconds", solutionTime);

        if (printSol)
        {
            instance->printSolutionToStdout();
        }

        if (!resultFile.empty())
        {
            copt::SolutionWriter solutionWriter(modelInstance, instanceCreationTime, solutionTime, solverName);

            solutionWriter.write(resultFile);
        }
    }

    return 0;
}