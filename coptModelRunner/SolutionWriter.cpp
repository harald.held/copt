#include "SolutionWriter.h"

#include <MilpModel.h>
#include <StringConversion.h>

#include <fmt/core.h>
#include <yaml-cpp/yaml.h>

#include <fstream>

namespace copt
{

SolutionWriter::SolutionWriter(const MilpModel *model, double timeModelCreation, double timeSolving,
                               std::string_view solver)
    : model_(model)
    , timeModelCreation_(timeModelCreation)
    , timeSolving_(timeSolving)
    , solver_(solver)
{}

void SolutionWriter::write(std::string_view fileName) const
{
    std::ofstream outFile(fileName.data(), std::ios::trunc);

    YAML::Node root;
    YAML::Node info;
    YAML::Node vars;

    info["solver"]                            = solver_.data();
    info["instance creation time in seconds"] = fmt::format("{:.3f}", timeModelCreation_);
    info["solution time in seconds"]          = fmt::format("{:.3f}", timeSolving_);

    const auto variables = model_->vars();

    for (const auto *v : variables)
    {
        const auto varName  = v->name();
        const auto varType  = toString(v->varType());
        const auto varValue = v->solutionValue().value();

        if (std::abs(varValue) <= 1.E-9)
        {
            continue;
        }

        YAML::Node var;

        var["name"]  = varName.data();
        var["type"]  = varType;
        var["value"] = varValue;

        vars.push_back(var);
    }

    root["info"]      = info;
    root["variables"] = vars;

    outFile << root;
}

} // namespace copt
