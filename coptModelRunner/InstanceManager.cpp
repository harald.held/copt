#include "InstanceManager.h"

#include <ParameterReader.h>

#include <dlfcn.h>

namespace copt
{

namespace
{

bool parameterExists(const std::string &expectedParam, const std::vector<Parameter> &availableParams)
{
    for (const auto &p : availableParams)
    {
        if (p.name() == expectedParam)
        {
            return true;
        }
    }

    return false;
}

} // namespace

InstanceManager::InstanceManager(std::string_view instanceLib, std::string_view paramFile)
{
    loadInstance(instanceLib);
    loadAndCheckParameters(paramFile);
    instance_->buildModel();
}

InstanceManager::~InstanceManager()
{
    if (releaseFunc_)
    {
        releaseFunc_(instance_);
    }

    if (loadedLibInstance_)
    {
        dlclose(loadedLibInstance_);
    }
}

void InstanceManager::loadInstance(std::string_view instanceLib)
{
    loadedLibInstance_ = dlopen(instanceLib.data(), RTLD_LAZY);

    if (!loadedLibInstance_)
    {
        logger_->error("could not load instance '{}'", instanceLib);
        return;
    }

    if (auto *createFunc = dlsym(loadedLibInstance_, InstanceInterface::createFuncName))
    {
        auto *actualCreateFunc = reinterpret_cast<InstanceInterface::createInstanceFuncType>(createFunc);
        instance_              = actualCreateFunc();

        if (instance_)
        {
            logger_->info("instance '{}' successfully created", instanceLib);
        }
        else
        {
            logger_->error("could not create an instance of '{}' successfully", instanceLib);
        }
    }

    if (auto *releaseFunc = dlsym(loadedLibInstance_, InstanceInterface::releaseFuncName))
    {
        releaseFunc_ = reinterpret_cast<InstanceInterface::releaseInstanceFuncType>(releaseFunc);
    }
}

void InstanceManager::loadAndCheckParameters(std::string_view paramFile)
{
    if (!instance_)
    {
        logger_->error("cannot load parameters without having a valid instance");
        return;
    }

    ParameterReader paramReader(paramFile.data());
    const auto &    availableParams = paramReader.params();

    // check if we have all parameters the model wants
    const auto expectedParams = instance_->expectedParameters();

    for (const auto &p : expectedParams)
    {
        if (!parameterExists(p, availableParams))
        {
            logger_->error("missing required model parameter '{}' in parameter file '{}'", p, paramFile);
            return;
        }
    }

    for (auto p : availableParams)
    {
        instance_->setParameter(std::move(p));
    }

    logger_->info("model parameters set from parameter file '{}'", paramFile);
}

InstanceInterface *InstanceManager::instance() const
{
    return instance_;
}

} // namespace copt
