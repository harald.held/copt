#ifndef LOGGER_H
#define LOGGER_H

#include <memory>

#include <spdlog/logger.h>

namespace copt
{

std::shared_ptr<spdlog::logger> logger();

} // namespace copt

#endif // LOGGER_H
