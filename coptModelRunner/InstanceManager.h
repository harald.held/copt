#ifndef COPT_COPTMODELRUNNER_INSTANCEMANAGER_H
#define COPT_COPTMODELRUNNER_INSTANCEMANAGER_H

#include "logger.h"

#include <InstanceInterface.h>

#include <string_view>

namespace copt
{

class InstanceManager final
{
public:
    InstanceManager(std::string_view instanceLib, std::string_view paramFile);
    ~InstanceManager();

    [[nodiscard]] InstanceInterface *instance() const;

private:
    std::shared_ptr<spdlog::logger> logger_ = logger();

    void *                                     loadedLibInstance_ = nullptr;
    InstanceInterface *                        instance_          = nullptr;
    InstanceInterface::releaseInstanceFuncType releaseFunc_       = nullptr;

    void loadInstance(std::string_view instanceLib);
    void loadAndCheckParameters(std::string_view paramFile);
};

} // namespace copt

#endif // COPT_COPTMODELRUNNER_INSTANCEMANAGER_H
