#ifndef COPT_COPTMODELRUNNER_SOLUTIONWRITER_H
#define COPT_COPTMODELRUNNER_SOLUTIONWRITER_H

#include <string_view>

namespace copt
{

class MilpModel;

class SolutionWriter final
{
public:
    SolutionWriter(const MilpModel *model, double timeModelCreation, double timeSolving, std::string_view solver);

    void write(std::string_view fileName) const;

private:
    const MilpModel *model_;
    const double     timeModelCreation_;
    const double     timeSolving_;
    std::string_view solver_;
};

} // namespace copt

#endif // COPT_COPTMODELRUNNER_SOLUTIONWRITER_H
